# Co-op Dungeon Crawler #

This project was one I worked on in my spare time during 2015 and 2016. The game was intended to be an instanced networked multiplayer dungeon crawler/racer, where the aim was to get into teams of 4 and race to the end of an initially randomly generated dungeon. You'd keep your character progress between dungeons and so you would be able to build your character up over time. The dungeon would also be playable by other players after it was initially explored. So the plan of the game was to build up on low level dungeons until you could reach the vanguard and be part of the players who were exploring the new territories.

Gameplay wise, it was going to be a tactical brawler, so you'd be able to perform combinations based on the weapon you equipped, but with a focus on the speed, survival, and exploration elements; I wanted the game to be more about traversing the dungeons and less about combat.

The game requires a server to be run and then players can join that hosted game. I also began work on a Goal Oriented AI for the giant rat creature you can see in the gif below. This is because the plan was for enemies to exist in their own ecosystem, and players could use knowledge about their habits to lead them to food and water or hunt and eat them.

There was the beginnings of a skill menu, for combat and non-combat abilities that could be performed while combat was running. I don't know how usable that would've been but I like the concept.

Needless to say this was wildly out of scope for me! But I learnt so much from the project. This was the first time I had done more advanced modelling and animation linked to more complex input (the 1-2 punch combo). I also iterated on my network code from the Spoonie Island project.

It's still a game idea I have a lot of love for, and maybe one day I'll try again with the knowledge I've gained.

Here's what it turned out as (gif recorded at 15fps!):

![Alt Text](https://media.giphy.com/media/tJgwCAPj4YHh68aBjg/giphy.gif)