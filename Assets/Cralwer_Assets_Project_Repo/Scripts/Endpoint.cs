﻿/*using UnityEngine;
using System.Collections;

public class Endpoint : MonoBehaviour 
{
	private GameMaster m_GameMaster;
	public bool debugLoggingEnabled;

	void Start () 
	{
		getGameMaster ();
	}

	void OnCollisionEnter(Collision collision)
	{
		GameObject goPlayer = collision.gameObject;
		debugLog ("EndPoint Collided with object.", goPlayer);

		if (goPlayer.CompareTag ("Player")) 
		{
			PlayerScript player = goPlayer.GetComponent<PlayerScript>();
			if(player.Equals(null))
			{
				errorLog("Could not find Player script on player colliding with end.");
				return;
			}
			m_GameMaster.playerReachedEnd(player);
		}
	}

	void getGameMaster()
	{
		GameObject goGameMaster = GameObject.Find ("GameMaster");
		if(goGameMaster.Equals(null))
		{
			errorLog("Could not find GsmeMaster object.");
			return;
		}
		m_GameMaster = goGameMaster.GetComponent<GameMaster> ();
		if(m_GameMaster.Equals(null))
		{
			errorLog("Could not find GsmeMaster script.");
		}
	}

	void debugLog(string logMessage)
	{
		if (debugLoggingEnabled) 
		{
			Debug.Log (logMessage, gameObject);
		}
	}
	
	void errorLog(string logMessage)
	{
		if (debugLoggingEnabled) 
		{
			Debug.LogError(logMessage, gameObject);
		}
	}
}
*/