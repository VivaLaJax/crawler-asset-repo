﻿namespace AnimationDefs
{
	public enum ControllerRequest
	{
		REQ_NONE,
		REQ_PLAY,
		REQ_REPEAT,
		REQ_STOP
	};
}
