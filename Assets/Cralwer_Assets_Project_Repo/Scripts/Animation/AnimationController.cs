﻿using UnityEngine;
using System.Collections;

using AnimationDefs;

public class AnimationController : MonoBehaviour 
{
	public AnimationClip m_CurrClip;
	public float m_nClipPlaySpeed;
	
	private Animation m_Animation;
	
	private bool m_bAnimComplete;
	private bool m_bTimerStarted;
	
	public int m_nCurrentState;
	public enum ControllerState
	{
		STATE_IDLE,
		STATE_PLAY,
		STATE_REPEAT
	};
	
	const float CROSS_FADE_TIME = 0.03f;
	public int m_nCurrentRequest;
	
	void Start () 
	{
		m_Animation = GetComponentInChildren<Animation>();
		m_nCurrentState = (int) ControllerState.STATE_IDLE;
		m_bTimerStarted = false;
		m_bAnimComplete = false;
		m_nCurrentRequest = (int)ControllerRequest.REQ_NONE;
	}
	
	void FixedUpdate () 
	{
		switch(m_nCurrentRequest)
		{
		case (int) ControllerRequest.REQ_NONE:
			{
				//Empty but not unknown request
				break;
			}
		case (int) ControllerRequest.REQ_PLAY:
			{
				m_nCurrentState = (int)ControllerState.STATE_PLAY;
				break;
			}
		case (int) ControllerRequest.REQ_REPEAT:
			{
				m_nCurrentState = (int)ControllerState.STATE_REPEAT;
				break;
			}
		case (int) ControllerRequest.REQ_STOP:
			{
				m_nCurrentState = (int)ControllerState.STATE_IDLE;
				break;
			}	
		default:	
			{
				Debug.LogWarning("Unknown animation request. Yuck!");
				break;
			}
		}
		
		m_nCurrentRequest = (int)ControllerRequest.REQ_NONE;
		
		switch(m_nCurrentState)
		{
		case (int) ControllerState.STATE_IDLE:
			{
				m_Animation.Stop();
				break;
			}
		case (int) ControllerState.STATE_PLAY:
			{
				if(m_CurrClip!=null)
				{
					m_Animation[m_CurrClip.name].speed = m_nClipPlaySpeed;
					//m_Animation.Play(m_CurrClip.name);
					m_Animation.CrossFade(m_CurrClip.name,CROSS_FADE_TIME);	
				
					if(!m_bTimerStarted)
					{
						m_bTimerStarted = true;
						StartCoroutine(AnimationTimer(m_CurrClip.length * m_nClipPlaySpeed));
					}
					
					if(m_bAnimComplete)
					{
						m_bTimerStarted = false;
						MakeRequest("", 0, (int)ControllerRequest.REQ_STOP);	
					}
				}
				else
				{
					MakeRequest("", 0, (int)ControllerRequest.REQ_STOP);
				}
				break;
			}
		case (int) ControllerState.STATE_REPEAT:
			{
				if(m_CurrClip!=null)
				{
					m_Animation[m_CurrClip.name].speed = m_nClipPlaySpeed;
					m_Animation.CrossFade(m_CurrClip.name,CROSS_FADE_TIME);	
				}
				else
				{
					MakeRequest("", 0, (int)ControllerRequest.REQ_STOP);
				}
				break;
			}
		default:
			{
				Debug.LogWarning("No animation state set! Ahh!!");
				break;
			}
		}
		
	}
	
	public void MakeRequest(string strAnimName, float nAnimSpeed, int nRequest)
	{
		if(m_CurrClip!=null)
		{
			if(m_CurrClip.name==strAnimName || m_bTimerStarted)
			{
				return;
			}
		}
		
		m_nCurrentRequest = nRequest;
		m_bTimerStarted = false;
		m_bAnimComplete = false;
		m_CurrClip = m_Animation.GetClip(strAnimName);
		m_nClipPlaySpeed = nAnimSpeed;
		
		NetworkManagerScript netManScript = GameObject.FindGameObjectWithTag("NetworkManager")
			.GetComponent<NetworkManagerScript>();
		GetComponent<NetworkView>().RPC ("MakeNetworkRequest", netManScript.getServerInfo(),strAnimName, nAnimSpeed,
						nRequest, Network.player);
	}
	
	[RPC]
	public void MakeNetworkRequest(string strAnimName, float nAnimSpeed, int nRequest, NetworkPlayer player)
	{
		if(Network.isServer)
		{
			for(int i=0; i<Network.connections.Length; i++)
			{
				if(Network.connections[i]!=player)
				{
					GetComponent<NetworkView>().RPC("MakeNetworkRequest",Network.connections[i],strAnimName, nAnimSpeed,
						nRequest, player);
				}
			}
		}
		
		m_nCurrentRequest = nRequest;
		m_bTimerStarted = false;
		m_bAnimComplete = false;
		m_CurrClip = m_Animation.GetClip(strAnimName);
		m_nClipPlaySpeed = nAnimSpeed;
	}
	
	IEnumerator AnimationTimer(float waitTime)
	{
		m_bAnimComplete = false;
		yield return new WaitForSeconds(waitTime);
    	m_bAnimComplete = true;
    }
}
