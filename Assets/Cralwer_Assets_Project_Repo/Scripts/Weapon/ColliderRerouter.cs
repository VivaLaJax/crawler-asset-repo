﻿using UnityEngine;
using System.Collections;

public class ColliderRerouter : MonoBehaviour 
{
	public WeaponSlotScript m_slot;
	
	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag!="Player")
		{
			m_slot.RerouteCollision(other);
		}
	}	
}