﻿using UnityEngine;
using System.Collections;

public class WeaponSlotScript : MonoBehaviour 
{
	public Weapon m_WeaponScript;
	
	public void RerouteCollision(Collider other)
	{
		m_WeaponScript.Hit (other.gameObject);
	}
	
	public void RerouteCollision(Collision collision)
	{
		m_WeaponScript.Hit(collision.gameObject);
	}
	
	public Weapon Equip(string strWeaponName)
	{
		//remove old component if necessary
		if(m_WeaponScript != null)
		{
			m_WeaponScript.Unequip();
			m_WeaponScript = null;
		}

        //add new component
        m_WeaponScript = WeaponFactory(strWeaponName);
		m_WeaponScript.SetUp();
		
		return m_WeaponScript;
	}

    public Weapon WeaponFactory(string weaponName)
    {
        if(weaponName.Equals("BareFisted"))
        {
            return (Weapon)gameObject.AddComponent<BareFisted>();
        }
        if (weaponName.Equals("HandToHand"))
        {
            return (Weapon)gameObject.AddComponent<HandToHand>();
        }

        return null;
    }
}
