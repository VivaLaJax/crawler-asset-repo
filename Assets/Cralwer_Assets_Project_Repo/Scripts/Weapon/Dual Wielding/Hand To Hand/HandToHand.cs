﻿using UnityEngine;
using System.Collections;

public class HandToHand : DualWieldedWeapon
{
	public AnimationClip m_AnimPunchLClip;
	public AnimationClip m_AnimPunchRClip;
	
	protected virtual void Start () 
	{
		setWeaponType("Hand to Hand");
		setModSpeed(0);
		setModDamage(0);
		setModCooldown(0);
		
		setBaseSpeed(1.0f);
		setBaseDamage(5.0f);
		setBaseCooldown(0.8f);
		
		setMaxInputCombo(2);
		setInputCount(0);
		setCooldownTimer((float)TimerDefs.TIMER_READY);
		setInputTimer((float)TimerDefs.TIMER_READY);
		setInputTimerIsActive(false);
		
		m_AnimPunchLClip = transform.parent.GetComponentInChildren<Animation>().GetClip("Punch_Left");
		m_AnimPunchRClip = transform.parent.GetComponentInChildren<Animation>().GetClip("Punch_Right");
	}
	
	void FixedUpdate () 
	{
		if(getCooldownTimer()==(float)TimerDefs.TIMER_READY || getCooldownTimer()==(float)TimerDefs.TIMER_DONE)
		{
			DisableColliders();
		}
		
		if(getInputTimerIsActive())
		{
			//if it's active, check it
			if(getInputTimer()==(float)TimerDefs.TIMER_DONE)
			{
				//reset input counts
				setInputTimerIsActive(false);
				setInputCount(0);
			}
		}
		else
		{
			//if you can attack, this is a combo attack and the timer isn't active
			if(getCooldownTimer()==(float)TimerDefs.TIMER_DONE && getInputCount()>0)
			{
				setCooldownTimer((float)TimerDefs.TIMER_READY);
				setInputTimerIsActive(true);
				StartCoroutine(StartInputTimer(0.5f));
			}
		}
	}
	
	public override void NormalAttack(float nStrength)
	{		
		//if out of cool down
		if(getCooldownTimer()==(float)TimerDefs.TIMER_DONE || getCooldownTimer()==(float)TimerDefs.TIMER_READY)
		{
			AnimationController animController = getAnimationController();
			//if first punch
			if(getInputCount()==0)
			{
				EnableCollider((int)tCollider.COLLIDER_LEFT);
				if(animController!=null)
				{
					animController.MakeRequest(m_AnimPunchLClip.name, 1.0f, 
								(int)AnimationDefs.ControllerRequest.REQ_PLAY);
				}
			}
			else if(getInputCount()==1)
			{
				EnableCollider((int)tCollider.COLLIDER_RIGHT);
				if(animController!=null)
				{
					animController.MakeRequest(m_AnimPunchRClip.name, 1.0f, 
								(int)AnimationDefs.ControllerRequest.REQ_PLAY);
				}
			}
			
			setInputCount((getInputCount()+1)%getMaxInputCombo());
			StartCoroutine(StartCooldownTimer(getCalculatedCooldown()));
		}
	}
	
	public override void Hit(GameObject goHit)
	{
		DisableColliders();
		if(goHit.tag=="Enemy")
		{
			goHit.GetComponent<TargetDummy>().OnHit(getCalculatedDamage());
		}
	}
}
