﻿using UnityEngine;
using System.Collections;

public class BareFisted : HandToHand 
{
	protected override void Start () 
	{
		base.Start();
		setWeaponName("Bare Fist");
		
		setModSpeed(-0.5f);
		setModDamage(-0.5f);
		setModCooldown(-0.5f);
	}
	
	public override void SetUp()
	{
		setAnimationController(transform.parent.GetComponent<AnimationController>());
		setLeftCollider(GameObject.Find("Fist_L").GetComponent<Collider>());
		setRightCollider(GameObject.Find("Fist_R").GetComponent<Collider>());
		
		DisableColliders();
		//set up tags
		m_CollLeft.gameObject.tag = "ActiveWeapon";
		m_CollRight.gameObject.tag = "ActiveWeapon";
	}
	
	public override void Unequip()
	{
		//unset tags
		m_CollLeft.gameObject.tag = "InactiveWeapon";
		m_CollRight.gameObject.tag = "InactiveWeapon";
		
		Destroy(this);
	}
}
