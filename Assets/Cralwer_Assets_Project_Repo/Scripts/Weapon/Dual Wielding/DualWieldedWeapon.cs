﻿using UnityEngine;
using System.Collections;

public class DualWieldedWeapon : Weapon 
{
	protected Collider m_CollLeft;
	protected Collider m_CollRight;
	
	public enum tCollider
	{
		COLLIDER_LEFT,
		COLLIDER_RIGHT
	}
	
	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}
	
	public Collider getLeftCollider()
	{
		return m_CollLeft;
	}
	
	public void setLeftCollider(Collider lColl)
	{
		m_CollLeft = lColl;
	}
	
	public Collider getRightCollider()
	{
		return m_CollRight;
	}
	
	public void setRightCollider(Collider rColl)
	{
		m_CollRight = rColl;
	}
	
	protected void EnableColliders()
	{
		m_CollLeft.enabled = true;
		m_CollRight.enabled = true;
	}
	
	protected void EnableCollider(int nColl)
	{
		if(nColl==0)
		{
			m_CollLeft.enabled = true;
		}
		else if(nColl==1)
		{
			m_CollRight.enabled = true;
		}
	}
	
	protected void DisableColliders()
	{
		m_CollLeft.enabled = false;
		m_CollRight.enabled = false;
	}
	
	protected void DisableCollider(int nColl)
	{
		if(nColl==0)
		{
			m_CollLeft.enabled = false;
		}
		else if(nColl==1)
		{
			m_CollRight.enabled = false;
		}
	}
}
