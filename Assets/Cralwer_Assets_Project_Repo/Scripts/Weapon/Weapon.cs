﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour 
{
	public enum TimerDefs
	{
		TIMER_READY = 9998,
		TIMER_DONE = 9999
	};
	
	protected float m_nBaseDamage;
	protected float m_nModDamage;
	
	protected float m_nBaseSpeed;
	protected float m_nModSpeed;
	
	protected float m_nBaseCooldown;
	protected float m_nModCooldown;
	
	protected float m_nCooldownTimer;
	
	protected float m_nInputTimer;
	protected int m_nInputCount;
	protected int m_nMaxInputCombo;
	protected bool m_bInputTimerIsActive;
	
	protected string m_strWeaponType;
	protected string m_strWeaponName;
	
	protected AnimationController m_animController;
	
	public virtual void Hit(GameObject goHit) {}
	public virtual void NormalAttack(float nStrength) {}
	public virtual void SetUp() {}
	public virtual void Unequip() {}
	
	public float getBaseDamage()
	{
		return m_nBaseDamage;
	}
	
	public void setBaseDamage(float nVal)
	{
		m_nBaseDamage = nVal;
	}
	
	public float getModDamage()
	{
		return m_nModDamage;
	}
	
	public void setModDamage(float nVal)
	{
		m_nModDamage = nVal;
	}
	
	public float getCalculatedDamage()
	{
		return m_nBaseDamage + m_nModDamage;
	}
	
	public float getBaseSpeed()
	{
		return m_nBaseSpeed;
	}
	
	public void setBaseSpeed(float nVal)
	{
		m_nBaseSpeed = nVal;
	}
	
	public float getModSpeed()
	{
		return m_nModSpeed;
	}
	
	public void setModSpeed(float nVal)
	{
		m_nModSpeed = nVal;
	}
	
	public float getCalculatedSpeed()
	{
		return m_nBaseSpeed + m_nModSpeed;
	}
	
	public float getBaseCooldown()
	{
		return m_nBaseCooldown;
	}
	
	public void setBaseCooldown(float nVal)
	{
		m_nBaseCooldown = nVal;
	}
	
	public float getModCooldown()
	{
		return m_nModCooldown;
	}
	
	public void setModCooldown(float nVal)
	{
		m_nModCooldown = nVal;
	}
	
	public float getCalculatedCooldown()
	{
		return m_nBaseCooldown + m_nModCooldown;
	}
	
	public string getWeaponType()
	{
		return m_strWeaponType;
	}
	
	public void setWeaponType(string strType)
	{
		m_strWeaponType = strType;
	}
	
	public string getWeaponName()
	{
		return m_strWeaponName;
	}
	
	public void setWeaponName(string strName)
	{
		m_strWeaponName = strName;
	}
	
	public void setCooldownTimer(float nCooldownTimer)
	{
		m_nCooldownTimer = nCooldownTimer;
	}
	
	public float getCooldownTimer()
	{
		return m_nCooldownTimer;
	}
	
	public void setInputTimer(float nInputTimer)
	{
		m_nInputTimer = nInputTimer;
	}
	
	public float getInputTimer()
	{
		return m_nInputTimer;
	}
	
	public void setInputCount(int nInputCount)
	{
		m_nInputCount = nInputCount;
	}
	
	public int getInputCount()
	{
		return m_nInputCount;
	}
	
	public void setMaxInputCombo(int nMaxCombo)
	{
		m_nMaxInputCombo = nMaxCombo;
	}
	
	public int getMaxInputCombo()
	{
		return m_nMaxInputCombo;
	}
	
	public void setInputTimerIsActive(bool bActive)
	{
		m_bInputTimerIsActive = bActive;
	}
	
	public bool getInputTimerIsActive()
	{
		return m_bInputTimerIsActive;
	}
	
	public AnimationController getAnimationController()
	{
		return m_animController;
	}
	
	public void setAnimationController(AnimationController animController)
	{
		m_animController = animController;	
	}
	
	public IEnumerator StartCooldownTimer(float nTime)
	{
		setCooldownTimer(nTime);
		yield return new WaitForSeconds(getCooldownTimer());
    	setCooldownTimer((float)TimerDefs.TIMER_DONE);
    }
	
	public IEnumerator StartInputTimer(float nTime)
	{
		setInputTimer(nTime);
		yield return new WaitForSeconds(getInputTimer());
		setInputTimer((float)TimerDefs.TIMER_DONE);
    }
}
