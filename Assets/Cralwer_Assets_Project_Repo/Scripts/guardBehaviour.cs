using UnityEngine;
using System.Collections;

public class guardBehaviour : MonoBehaviour 
{
	private Vector3 startPos;
	private Vector3 endPos;
	private Transform myTransform;
	public GameObject target;
	
	private float i;
	private bool seen;
	
	// Use this for initialization
	void Start () 
	{
		myTransform = transform;
		i = 0;
		seen = false;
		startPos = myTransform.position;
		endPos = new Vector3(target.transform.position.x, myTransform.position.y, target.transform.position.z);
		myTransform.LookAt(endPos);
	}
	
	// Update is called once per frame
	void Update () 
	{
		float angle;
		Vector3 localDir;
		Ray ray;
		RaycastHit hit;
		
		for(int j=0; j<5; j++)
		{
			angle = 50.0f - j*20;
			localDir = new Vector3(Mathf.Sin(Mathf.Deg2Rad * angle), 0, Mathf.Cos(Mathf.Deg2Rad * angle));
			ray = new Ray(myTransform.position, myTransform.TransformDirection(localDir));
			
			Debug.DrawRay(ray.origin, ray.direction, Color.green);
			if(Physics.Raycast(ray, out hit, 20f))
			{
				GameObject wasHit = hit.collider.gameObject;
				if(wasHit.tag=="Player")
				{
					seeChange();
				}
			}
		}
		
		if(i>=1)
		{
			endPos = startPos;
			startPos = myTransform.position;
			myTransform.LookAt(endPos);
			i=0;
		}
		else
		{
			i+=0.01f;
			transform.position = Vector3.Lerp(startPos, endPos, i);
			Debug.DrawRay(myTransform.position, myTransform.forward);
		}
	}
	
	void seeChange()
	{
		seen = true;
	}
	
	void OnGUI()
	{
		if(seen)
		{
			int width = 150;
			int height = 90;
			int x = Screen.width/2 - width/2;
			int y = Screen.height/2 - height/2;
			
			GUI.Box(new Rect(x,y,width,height), "You've been seen!");

			if(GUI.Button(new Rect(x+10,y+30,width-20,height-70), "Main Menu")) 
			{
				Application.LoadLevel("Level_Select");
			}

			if(GUI.Button(new Rect(x+10,y+60,width-20,height-70), "Quit")) 
			{
				Application.Quit();
			}
		}
	}
}


