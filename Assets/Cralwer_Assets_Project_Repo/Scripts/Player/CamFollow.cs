using UnityEngine;
using System.Collections;

public class CamFollow : MonoBehaviour 
{
	public GameObject play;
	Transform myTransform;
	public float m_nCamHeight;
	public float m_nCamDepth;
	
	// Use this for initialization
	void Start () 
	{
		myTransform = transform;
	}
	
	// Update is called once per frame
	void Update () 
	{				
		if(play)
		{
			Vector3 playX = new Vector3(play.transform.position.x,play.transform.position.y+m_nCamHeight,
				play.transform.position.z+m_nCamDepth);
			myTransform.position = Vector3.Lerp(myTransform.position,playX,0.9f);
		}
	}
	
	public void setPlay(GameObject player)
	{
		play = player;
	}
}
