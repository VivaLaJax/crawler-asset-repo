using UnityEngine;
using System.Collections;

public class clickToTarget : MonoBehaviour 
{
	private Ray ray;
    private RaycastHit hit;
	private Transform myTransform;
	
	// Use this for initialization
	void Start () 
	{
		myTransform = transform;
	}
	
	// Update is called once per frame
	void Update () 
	{
		//PlayerCharacterScript playerScript = GetComponent<PlayerCharacterScript>();
		//if(Network.player==playerScript.GetOwner())
		if(Network.isClient)
		{
			if(Camera.main!=null)
			{
				ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				
		   		//this if check for the mouse left click
		   		if (Input.GetMouseButton(0)) 
		   		{
					// Moves the Player if the Left Mouse Button was clicked
					Plane playerPlane = new Plane(Vector3.up, myTransform.position);
					float hitdist = 0.0f;
		 
					if (playerPlane.Raycast(ray, out hitdist)) 
					{
						Vector3 targetPoint = ray.GetPoint(hitdist);
						Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);
						myTransform.rotation = targetRotation;
					}
		   		}
			}
		}
	}
}
