using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerCharacterScript : MonoBehaviour 
{
	NetworkPlayer myOwner;
	
	private int currentState;
	private int currLevel;
	
	public GameObject blockPref;
	public GameObject movingBlockPref;
	
	public GameObject cam;
	private GameObject myCam;
	
	public int m_nHealth;
	public int MAX_HEALTH;
	public int m_nConcentration;
	public int MAX_CONCENTRATION;
	public int m_nHunger;
	public int MAX_HUNGER;
	public int m_nThirst;
	public int MAX_THIRST;
	public int m_nFatigue;
	public int MAX_FATIGUE;
	
	public bool m_bInActionMenu;
	
	private MovementAction moveActionSlot;
	private ExecutableAction execActionSlot;
	
	List<ActionClass> m_LstAvailableActions;
	
	bool m_bBuildMenu;
	
	enum actionButtonState
	{
		CREATE_STATIC_OBJECT,
		CREATE_MOBILE_OBJECT,
		MOVE_UP_LEVEL,
		MOVE_DOWN_LEVEL
	}
	
	//Animations
	public AnimationClip m_AnimWalkClip;
	public AnimationClip m_AnimPunchLClip;
	public AnimationClip m_AnimPunchRClip;
	
	public Weapon m_EquippedWeapon;
	const string WEAPON_DEFAULT = "BareFisted";
	
	void Awake()
	{
		myCam = (GameObject)Instantiate(cam);
		myCam.GetComponent<CamFollow>().setPlay(gameObject);
	}
	
	void Start () 
	{
		currentState = 0;
		m_bInActionMenu = false;
		m_LstAvailableActions = new List<ActionClass>();
		m_LstAvailableActions.Add(gameObject.AddComponent<Dash>());
		m_LstAvailableActions.Add(gameObject.AddComponent<PowerShot>());
		
		SetMovementSlot(m_LstAvailableActions[0].GetName());
		SetExecSlot(m_LstAvailableActions[1].GetName());
	
		m_bBuildMenu = true;
		
		m_EquippedWeapon = GetComponentInChildren<WeaponSlotScript>().Equip(WEAPON_DEFAULT);
	}
	
	void Update () 
	{		
		if(Network.player==GetOwner())
		{			
			if(Input.GetKeyDown(KeyCode.Alpha1))
			{
				Debug.Log("Player Character State: Create Static");
				currentState = (int) actionButtonState.CREATE_STATIC_OBJECT;
			}
			else if(Input.GetKeyDown(KeyCode.Alpha2))
			{
				Debug.Log("Player Character State: Create Mobile");
				currentState = (int) actionButtonState.CREATE_MOBILE_OBJECT;
			}
			else if(Input.GetKeyDown(KeyCode.Alpha3))
			{
				Debug.Log("Player Character State: Move Up");
				currentState = (int) actionButtonState.MOVE_UP_LEVEL;
			}
			else if(Input.GetKeyDown(KeyCode.Alpha4))
			{
				Debug.Log("Player Character State: Move Down");
				currentState = (int) actionButtonState.MOVE_DOWN_LEVEL;
			}
			
			if(Input.GetKeyDown(KeyCode.Space))
			{
				NetworkManagerScript netManScript = GameObject.FindGameObjectWithTag("NetworkManager").GetComponent<NetworkManagerScript>();
				
				switch(currentState)
				{
				case (int) actionButtonState.CREATE_STATIC_OBJECT:
					{
						Vector3 addOn = new Vector3(0,0,-transform.localScale.z);
						GetComponent<NetworkView>().RPC("requestStaticObj",netManScript.getServerInfo(),transform.position+addOn);
						break;
					}
				case (int) actionButtonState.CREATE_MOBILE_OBJECT:
					{
						Vector3 addOn = new Vector3(0,0,transform.localScale.z);
						GetComponent<NetworkView>().RPC("requestMobileObj",netManScript.getServerInfo(),transform.position+addOn);
						break;
					}
				case (int) actionButtonState.MOVE_UP_LEVEL:
					{
						netManScript.RequestChangeLevelIndex(currLevel, ++currLevel, Network.player);
						break;
					}
				case (int) actionButtonState.MOVE_DOWN_LEVEL:
					{
						if(currLevel>0)
						{
							netManScript.RequestChangeLevelIndex(currLevel, --currLevel, Network.player);
						}
						break;
					}
				}
			}
			
			if(Input.GetButtonDown("NormalAttack"))
			{
				if(m_EquippedWeapon!=null)
				{
					m_EquippedWeapon.NormalAttack(0);
				}
				/*AnimationController animControl = GetComponent<AnimationController>();
				animControl.MakeRequest(m_AnimPunchLClip.name, 1.0f, 
								(int)AnimationDefs.ControllerRequest.REQ_PLAY);*/
			}
			
			if(Input.GetButtonDown("ActionMenu"))
			{
				actionMenuToggled();
			}
			
			
			if(!m_bInActionMenu)
			{
				if(Input.GetButtonDown("MovementAction"))
				{
					moveActionSlot.executeAction();
				}
				
				if(Input.GetButtonDown("QueuedAction"))
				{
					execActionSlot.executeAction();
				}
			}
			
			if(m_bBuildMenu)
			{
				GetComponent<ActionMenuScript>().buildMenu();
				m_bBuildMenu = false;
			}
			
			if(moveActionSlot!=null)
			{
				moveActionSlot.Update();
			}
			
			if(execActionSlot!=null)
			{
				execActionSlot.Update();
			}
		}
	}
	
	////////////////////////////////////////////////////////////////////
	public void SetOwner(NetworkPlayer newOwner)
	{
		myOwner = newOwner;
		Debug.Log ("SetOwner: myOwner= " +myOwner.ToString());
		myCam.GetComponent<PlayerCamScript>().enableCamera(myOwner);
	}
	
	public NetworkPlayer GetOwner()
	{
		return myOwner;
	}
	
	public void SetCurrentLevel(int level)
	{
		currLevel = level;
	}
	
	public int GetCurrentLevel()
	{
		return currLevel;
	}
	
	public bool GetInActionMenu()
	{
		return m_bInActionMenu;
	}
	
	public void actionMenuToggled()
	{
		m_bInActionMenu = !m_bInActionMenu;
	}
	
	public void SetMovementSlot(string action)
	{
		int actionIndex = findIndexOfAction(action);
		if(actionIndex!=-99)
		{
			moveActionSlot = (MovementAction) m_LstAvailableActions[actionIndex];
			moveActionSlot.setTransform(transform);
		}
	}
	
	public void SetExecSlot(string action)
	{
		int actionIndex = findIndexOfAction(action);
		if(actionIndex!=-99)
		{
			execActionSlot = (ExecutableAction) m_LstAvailableActions[actionIndex];
		}
	}
	
	public List<ActionClass> GetAbilityList()
	{
		return m_LstAvailableActions;
	}
	
	public int findIndexOfAction(string action)
	{
		for(int i=0; i<m_LstAvailableActions.Count; i++)
		{
			if(m_LstAvailableActions[i].GetName()==action)
			{
				return i;
			}
		}
		
		return -99;
	}
	
	[RPC]
	void requestStaticObj(Vector3 pos)
	{
		createStaticObj(pos);
		for(int i=0; i<Network.connections.Length; i++)
		{
			GetComponent<NetworkView>().RPC("createStaticObj", Network.connections[i], pos);
		}
	}

	[RPC]
	void requestMobileObj(Vector3 pos)
	{
		NetworkViewID nextId = GameObject.FindGameObjectWithTag("NetworkManager").GetComponent<NetworkManagerScript>().GetNextId();
		
		createMobileObj(pos,nextId);
		for(int i=0; i<Network.connections.Length; i++)
		{
			GetComponent<NetworkView>().RPC("createMobileObj", Network.connections[i], pos, nextId);
		}
	}

	[RPC]
	void createStaticObj(Vector3 pos)
	{
		NetworkManagerScript netManScript = GameObject.FindGameObjectWithTag("NetworkManager").GetComponent<NetworkManagerScript>();
		
		GameObject block = (GameObject) Instantiate(blockPref, pos, Quaternion.identity);
		block.GetComponent<LevelAssetScript>().setLevelName(netManScript.GetLevelNameFromIndex(currLevel+2));
	}

	[RPC]
	void createMobileObj(Vector3 pos, NetworkViewID nextId)
	{
		NetworkManagerScript netManScript = GameObject.FindGameObjectWithTag("NetworkManager").GetComponent<NetworkManagerScript>();
		
		GameObject block = (GameObject) Instantiate(movingBlockPref, pos, Quaternion.identity);
		block.GetComponent<LevelAssetScript>().setLevelName(netManScript.GetLevelNameFromIndex(currLevel+2));
		
		NetworkView netView = block.GetComponent<NetworkView>();
		netView.viewID = nextId;
	}
}
