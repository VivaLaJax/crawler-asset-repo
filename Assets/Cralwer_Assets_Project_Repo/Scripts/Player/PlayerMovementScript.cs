using UnityEngine;
using System.Collections;

public class PlayerMovementScript : MonoBehaviour 
{
	public int speed = 15;
	public int gravity = 20;
	private Vector3 moveDirection = Vector3.zero;
	private CharacterController controller;
	private bool chatting;
	private Vector3 lastPosition;
	
	
	void Awake()
	{
		//DontDestroyOnLoad(this);
		chatting = false;
	}
	
	// Use this for initialization
	void Start () {
		controller=GetComponent<CharacterController>();
	}
	
	void FixedUpdate () 
	{
		PlayerCharacterScript playerScript = GetComponent<PlayerCharacterScript>();
		AnimationController animControl = GetComponent<AnimationController>();
		Debug.DrawRay(transform.position, transform.forward);
		if(Network.player==playerScript.GetOwner())
		{
			if (controller.isGrounded) 
			{
				if(!chatting) //&& !playerScript.GetInActionMenu())
				{
					if(Input.GetAxis ("Horizontal")!=0 || Input.GetAxis ("Vertical")!=0)
					{
						animControl.MakeRequest(playerScript.m_AnimWalkClip.name, 1.0f, 
							(int)AnimationDefs.ControllerRequest.REQ_REPEAT);
					}
	           		moveDirection = new Vector3(-Input.GetAxis("Horizontal"), 0, -Input.GetAxis("Vertical"));
				}
				else
				{
					animControl.MakeRequest("", 0, (int)AnimationDefs.ControllerRequest.REQ_STOP);
					moveDirection = new Vector3(0,0,0);	
				}
        	   	moveDirection *= speed;
			}
			moveDirection.y -= gravity * Time.deltaTime;
        	controller.Move(moveDirection * Time.deltaTime);
			
			if(transform.position!=lastPosition)
			{
				NetworkManagerScript netManScript = GameObject.FindGameObjectWithTag("NetworkManager").GetComponent<NetworkManagerScript>();
				GetComponent<NetworkView>().RPC ("updatePosition",netManScript.getServerInfo(),transform.position, Network.player);
			}
			
			lastPosition = transform.position;
		}
	}
	
	public void OnChatting()
	{
		chatting = !chatting;
	}
	
	[RPC]
	void updatePosition(Vector3 updatedPosition, NetworkPlayer player)
	{
		if(Network.isServer)
		{
			for(int i=0; i<Network.connections.Length; i++)
			{
				PlayerCharacterScript playerScript = gameObject.GetComponent<PlayerCharacterScript>();
				NetworkManagerScript netManScript = GameObject.FindGameObjectWithTag("NetworkManager").GetComponent<NetworkManagerScript>();
				if(Network.connections[i]!=player && netManScript.ArePlayersInSameLevel(Network.connections[i],netManScript.GetLevelNameFromIndex(playerScript.GetCurrentLevel()+2)))
				{
					GetComponent<NetworkView>().RPC ("updatePosition",Network.connections[i],transform.position, Network.player);
				}
			}
		}
		
		transform.position = updatedPosition;
	}
}
