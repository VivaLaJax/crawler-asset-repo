using UnityEngine;
using System.Collections;

public class LevelAssetScript : MonoBehaviour 
{
	public string attachedLevelName;
	
	public string getLevelName()
	{
		return attachedLevelName;
	}
	
	public void setLevelName(string level)
	{
		attachedLevelName = level;
	}
	
	public string getObjectName()
	{
		string levelName = getLevelName();
		return levelName+":"+gameObject.name;
	}
	
	void Start()
	{
		/*if(gameObject.GetComponent<NetworkView>()!=null)
		{
			if(networkView.isMine)
			{
				GameObject netMan = GameObject.FindGameObjectWithTag("NetworkManager");
				NetworkManagerScript netScript =  netMan.GetComponent<NetworkManagerScript>();
				netScript.requestViewId(getObjectName());
			}
		}*/
	}
	
	
}
