using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelManager : MonoBehaviour 
{
	ArrayList CharsInLevel;
	public string levelName;
	
	ArrayList networkedObjs;
	ArrayList createdObjs;
	NetworkManagerScript netManScript;
	
	public GameObject netSlotOne;
	
	public GameObject m_gofloorTilePrefab;
	
	public int m_nFloorWidth;
	public int m_nFloorHeight;
	
	public List<GameObject> m_lstLevelTiles;
	
	void Awake()
	{
		GameObject netMan = GameObject.FindGameObjectWithTag("NetworkManager");
		netManScript = netMan.GetComponent<NetworkManagerScript>();
		
		if(Network.isServer)
		{
			GetComponent<NetworkView>().viewID = netManScript.GetNextId();
		}
	}

	void Start()
	{
		networkedObjs = new ArrayList();
		createdObjs = new ArrayList();
		CharsInLevel = new ArrayList();
		
		//m_nFloorWidth = Random.Range(3,10);
		//m_nFloorHeight = Random.Range(3,10);
		//constructLevel();
		netManScript.levelManagerLoaded();
		
		if(netSlotOne!=null)
			networkedObjs.Add(netSlotOne);
	}
	
	public void addChar(NetworkPlayer character)
	{
		Debug.Log("Character added to Level: " + character);
		CharsInLevel.Add(character);
		Debug.Log("Num Characters in Level: " + CharsInLevel.Count);
		
		if(Network.isServer)
		{
			int charOfID = -99;
			GameObject[] playerChars = GameObject.FindGameObjectsWithTag("Player");
			for(int j=0; j<playerChars.Length; j++)
			{
				PlayerCharacterScript charScript = playerChars[j].GetComponent<PlayerCharacterScript>();
				if(charScript.GetOwner()==character)
				{
					charOfID = j;
				}
			}
			
			if(charOfID!=-99)
			{
				NetworkViewID id = playerChars[charOfID].GetComponent<NetworkView>().viewID;
	
				for(int i=0; i<CharsInLevel.Count; i++)
				{
					GetComponent<NetworkView>().RPC("CreateNetPlayer", (NetworkPlayer)CharsInLevel[i], id, character);
				}
			}
		}
	}
	
	public NetworkPlayer getChar(int index)
	{
		return (NetworkPlayer) CharsInLevel[index];
	}
	
	public NetworkViewID getViewId()
	{
		return GetComponent<NetworkView>().viewID;
	}
	
	public bool isCharacterInLevel(NetworkPlayer player)
	{
		return CharsInLevel.Contains(player);
	}
	
	//remove character from list
	public void removeChar(NetworkPlayer character)
	{
		CharsInLevel.Remove(character);
		Debug.Log (getLevelName()+" Count: " + CharsInLevel.Count);
		checkForEmpty();
		
		if(Network.isServer)
		{			
			for(int i=0; i<CharsInLevel.Count; i++)
			{
				GetComponent<NetworkView>().RPC("playerHasLeftLevel", (NetworkPlayer)CharsInLevel[i], character);
			}
		}
	}
	
	public string getLevelName()
	{
		return levelName;
	}
	
	public void checkForEmpty()
	{
		//if there are no characters left
		if(CharsInLevel.Count==0)
		{
			//delete all things related to the level
			netManScript.deleteLevelAssets(levelName);
		}
	}
	
	void Update()
	{
		
	}

	public void Setup()
	{
		Vector3 pos = transform.position;
      
		for(int i=0; i<networkedObjs.Count; i++)
		{
			GameObject netObj = (GameObject) Instantiate((GameObject)networkedObjs[i], pos, Quaternion.identity);
			netObj.GetComponent<NetworkView>().viewID = netManScript.GetNextId();
			createdObjs.Add(netObj);
		}
	}
	
	public void createNetObjs(NetworkPlayer player)
	{
		for(int i=0; i<networkedObjs.Count; i++)
		{
			NetworkViewID id = ((GameObject)createdObjs[i]).GetComponent<NetworkView>().viewID;
			Debug.Log("Sending RPC with index: " + i + " and id: " + id);
			GetComponent<NetworkView>().RPC("CreateNetObjByIndex", player, i, id);
		}
      
		GameObject[] playerChars = GameObject.FindGameObjectsWithTag("Player");
		
		for(int i=0; i<CharsInLevel.Count; i++)
		{	
			for(int j=0; j<playerChars.Length; j++)
			{
				PlayerCharacterScript charScript = playerChars[j].GetComponent<PlayerCharacterScript>();
				if(charScript.GetOwner()==((NetworkPlayer)CharsInLevel[i])&&charScript.GetOwner()!=player)
				{
					NetworkViewID id = charScript.GetComponent<NetworkView>().viewID;
					
					GetComponent<NetworkView>().RPC("CreateNetPlayer", player, id, charScript.GetOwner());
					break;
				}
			}
		}
	}
   
	[RPC]
	void CreateNetObjByIndex(int index, NetworkViewID newId)
	{
		Vector3 pos = transform.position;
		Debug.Log("Creating object: " + index);
		GameObject netObj = (GameObject) Instantiate((GameObject)networkedObjs[index], pos, Quaternion.identity);
		Debug.Log("Assigning view id: " + newId);
		netObj.GetComponent<NetworkView>().viewID = newId;
		createdObjs.Add(netObj);
	}

	[RPC]
	void CreateNetPlayer(NetworkViewID viewId, NetworkPlayer owner)
	{
		if(owner!=Network.player)
		{
			netManScript.SpawnPlayer(owner, viewId, levelName);
		}
	}

	[RPC]
	void playerHasLeftLevel(NetworkPlayer player)
	{
		if(player!=Network.player)
		{
			netManScript.DestroyPlayer(player);
		}
	}
	
	public void recieveId(NetworkViewID myId)
	{
		GetComponent<NetworkView>().viewID = myId;
	}
	
	private void constructLevel()
	{
		for(int i=0; i<m_nFloorWidth; i++)
		{
			for(int j=0; j<m_nFloorHeight; j++)
			{
				Vector3 vecPos = new Vector3(i,0,j);
				GameObject goTile = (GameObject)Instantiate(m_gofloorTilePrefab, vecPos, Quaternion.identity);
				
				goTile.GetComponent<LevelAssetScript>().setLevelName(levelName);
				
				m_lstLevelTiles.Add(goTile);
			}
		}
	}
}
