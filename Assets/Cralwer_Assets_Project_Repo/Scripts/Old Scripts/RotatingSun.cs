using UnityEngine;
using System.Collections;

public class RotatingSun : MonoBehaviour 
{
	private Quaternion lastRotation;
	private NetworkManagerScript netManScript;
	
	// Use this for initialization
	void Start () 
	{
		lastRotation = Quaternion.identity;
		netManScript = GameObject.FindGameObjectWithTag("NetworkManager").GetComponent<NetworkManagerScript>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Network.isServer)
		{
			gameObject.transform.RotateAround(new Vector3(0,0,0),new Vector3(0,0,1),100*Time.deltaTime);
			gameObject.transform.LookAt(new Vector3(0,0,0));
			
			if(transform.rotation!=lastRotation)
			{
				LevelManager levelMan = netManScript.getLevelManager(gameObject.GetComponent<LevelAssetScript>().getLevelName());
				for(int i=0; i<Network.connections.Length; i++)
				{
					if(levelMan.isCharacterInLevel(Network.connections[i]))
					{
						GetComponent<NetworkView>().RPC ("updatePosition",Network.connections[i],transform.position, transform.rotation);
					}
				}
			}
			
			lastRotation = transform.rotation;
		}
	}
	
	[RPC]
	void updatePosition(Vector3 updatedPosition, Quaternion updatedRotation)
	{
		if(!Network.isServer)
		{
			transform.position = updatedPosition;
			transform.rotation = updatedRotation;
		}
	}
}
