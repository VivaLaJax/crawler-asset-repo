using UnityEngine;
using System.Collections;

public class PlayerCamScript : MonoBehaviour 
{
	void Awake ()
	{
    	GetComponent<Camera>().enabled = false;
		GetComponent<AudioListener>().enabled = false;
	}
	
	public void enableCamera(NetworkPlayer owner)
	{
		if(Network.player==owner)
		{
    		GetComponent<Camera>().enabled = true;
			GetComponent<AudioListener>().enabled = true;
    	}
	}
}
