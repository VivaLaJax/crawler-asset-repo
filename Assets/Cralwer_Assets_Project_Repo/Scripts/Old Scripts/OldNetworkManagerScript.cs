/*using UnityEngine;
using System.Collections;

public class OldNetworkManagerScript : MonoBehaviour 
{
	string gameName = "VivaLaJax_Crows_Call";
	public GameObject playerPrefab;
	public GameObject camPrefab;
	public Transform spawnObject;
	private string doorSpawn;
	private bool refreshing = false;
	private HostData[] hostInfo;
	private float btnX;
	private float btnY;
	private float btnW;
	private float btnH;
	
	public string[] supportedNetworkLevels = { "LoginScreen","MasterLevel","CentralSquare","MagicTavern" };
	public ArrayList strArrLloadedLevels = new ArrayList();
	string  disconnectedLevel = "LoginScreen";
	private int lastLevelPrefix = 0;
	bool isLoadingLevel;
	bool hasJustComeThroughDoor;
	bool playerSpawnedYet;
	
	void Awake()
	{
		DontDestroyOnLoad(this);
		networkView.group = 1;
		isLoadingLevel = false;
		hasJustComeThroughDoor = false;
		playerSpawnedYet = false;
	}
	
	void Start()
	{
		btnX = (float)(Screen.width * 0.05);
		btnY = (float)(Screen.width * 0.05);
		btnW = (float)(Screen.width * 0.1);
		btnH = (float)(Screen.width * 0.1);
		hostInfo = null;
	}
	
	void Update()
	{
		if(refreshing)
		{
			if(MasterServer.PollHostList().Length>0)
			{
				refreshing = false;
				hostInfo = MasterServer.PollHostList();
			}
		}
	}
	
	void StartServer()
	{
		NetworkConnectionError error = Network.InitializeServer(32,251,!Network.HavePublicAddress());
		MasterServer.RegisterHost(gameName,"Learning Game: Jax","Tutorial Game");
		
		if(error.ToString()=="NoError")
		{
			NextLevel("MasterLevel");
		}
	}
	
	void RefreshHostList()
	{
		MasterServer.RequestHostList(gameName);
		refreshing = true;
	}
	
	void NextLevel(string level)
	{
		Network.RemoveRPCsInGroup(0);
		Network.RemoveRPCsInGroup(1);
		
		StartCoroutine(LoadLevel(level, getLevelPrefix(level)));
	}
	
	public void LoadLevelFromPortal(string destination, string source, GameObject player, NetworkPlayer netPlayer)
	{
		//Network.RemoveRPCsInGroup(0);
		//Network.RemoveRPCsInGroup(1);
		
		doorSpawn = source;
		hasJustComeThroughDoor = true;
		
		networkView.RPC("CheckIfLevelLoadedOnServer",RPCMode.Server,destination);
		networkView.RPC("CloseOffPrevLevelUpdates",RPCMode.Server,source,netPlayer);
		
		if(!isLevelLoaded(destination))
		{
			Debug.Log("Loading Level From Portal: Creating Additive Level");
			player.GetComponent<PlayerCharacter>().setCurrentLevel(destination,getLevelPrefix(destination));
			StartCoroutine(LoadLevelAdd(destination, getLevelPrefix(destination),player));
		}
		else
		{
			if(player!=null)
			{
				Debug.Log("Loading Level From Portal: Transporting Player");
				networkView.RPC("OpenUpPrevLevelUpdates",RPCMode.Server,destination,netPlayer);
				TransportPlayer(player);
				addPlayerToLevel(player.GetComponent<PlayerCharacter>().getName(),destination);
			}
		}
		
		deletePlayerFromLevel(player.GetComponent<PlayerCharacter>().getName(),source);
	}
	
	//[RPC]
	void SpawnPlayer()
	{
		Vector3 loadedPos = new Vector3(0,0,0);
		if(!SystemScript.getPositionFromArray(ref loadedPos))
		{
			spawnObject = GameObject.FindGameObjectWithTag("spawn").transform;
			loadedPos = spawnObject.position;
		}

		Network.Instantiate(playerPrefab, loadedPos, Quaternion.identity, 0);	
	}
	
	void TransportPlayer(GameObject player)
	{
		Vector3 loadedPos = new Vector3(0,0,0);
		
		loadedPos = getPortalSpawnPosition(doorSpawn);
		hasJustComeThroughDoor = false;
		
		player.transform.position = loadedPos;
	}
	
	void SpawnCamera()
	{
		spawnObject = GameObject.FindGameObjectWithTag("ServerCamSpawn").transform;
		Instantiate(camPrefab, spawnObject.position, spawnObject.rotation);
	}
	
	int getLevelPrefix(string level)
	{
		for(int i=0; i<supportedNetworkLevels.Length; i++)
		{
			if(supportedNetworkLevels[i]==level)
			{
				return i;
			}
		}
		
		return -99;
	}
	
	Vector3 getPortalSpawnPosition(string spawn)
	{
		Vector3 retPos = new Vector3(0,0,0);
		
		GameObject[] portals = GameObject.FindGameObjectsWithTag("Portal");
		
		for(int i=0; i<portals.Length; i++)
		{
			DoorWayScript door = portals[i].GetComponent<DoorWayScript>();
			if(door.getDestination()==spawn)
			{
				retPos = portals[i].transform.position+door.getBufferVector();
				return retPos;
			}
		}
		
		return retPos;
	}
	
	[RPC]
	void CheckIfLevelLoadedOnServer(string level)
	{
		if(Network.isServer)
		{
			if(!isLevelLoaded(level))
			{
				StartCoroutine(LoadLevelAdd(level, getLevelPrefix(level),null));
			}
		}
	}
	
	public bool isLevelLoaded(string level)
	{
		return strArrLloadedLevels.Contains(level);
	}
	
	public void addLevelToLoaded(string level)
	{
		strArrLloadedLevels.Add(level);
	}
	
	public void removeLevelFromLoaded(string level)
	{
		strArrLloadedLevels.Remove(level);
	}
	
	//GUI
	void OnGUI () 
	{
		if (!Network.isClient && !Network.isServer && Application.loadedLevelName=="LoginScreen")
		{
			LoginScreenScript login = (LoginScreenScript) GameObject.FindGameObjectWithTag("Login").GetComponent<LoginScreenScript>();
			
			if(login.getCurrentState()==2)
			{
				if(GUI.Button(new Rect(btnX,btnY,btnW,btnH), "Start Server"))
				{
					Debug.Log("Starting Server");
					StartServer();
				}
		
				if(GUI.Button(new Rect(btnX,(float)(btnY*1.2+btnH),btnW,btnH), "Refresh Hosts"))
				{
					Debug.Log("Refreshing");
					RefreshHostList();
				}
			
				if(hostInfo!=null)
				{
					if(hostInfo.Length>0)
					{
						for(int i=0; i<hostInfo.Length; i++)
						{
							if(GUI.Button(new Rect(btnX*1.5f + btnW,btnY*1.2f+(btnH*i),btnW*3,btnH*0.5f), hostInfo[i].gameName))
							{
								Network.Connect(hostInfo[i]);
							}
						}
					}
				}
			}
		}
	}
	
	//[RPC]
	public IEnumerator LoadLevel(string level, int levelPrefix)
	{
		isLoadingLevel = true;
		lastLevelPrefix = levelPrefix;

		// There is no reason to send any more data over the network on the default channel,
		// because we are about to load the level, thus all those objects will get deleted anyway
		Network.SetSendingEnabled(0, false);	

		// We need to stop receiving because first the level must be loaded first.
		// Once the level is loaded, rpc's and other state update attached to objects in the level are allowed to fire
		Network.isMessageQueueRunning = false;

		// All network views loaded from a level will get a prefix into their NetworkViewID.
		// This will prevent old updates from clients leaking into a newly created scene.
		Network.SetLevelPrefix(levelPrefix);
		Application.LoadLevel(level);
		Debug.Log ("Level loaded.");
		//yield return 0;
		//yield return 0;
		yield return StartCoroutine("waitForLoad");

		// Allow receiving data again
		Network.isMessageQueueRunning = true;
		// Now the level has been loaded and we can start sending out data to clients
		Network.SetSendingEnabled(0, true);

		addLevelToLoaded(level);
		
		foreach(GameObject go in FindObjectsOfType(typeof(GameObject)) as GameObject[])
		{
			go.SendMessage("OnNetworkLoadedLevel", level, SendMessageOptions.DontRequireReceiver);	
		}
	}
	
	public IEnumerator LoadLevelAdd(string level, int levelPrefix, GameObject player)
	{
		Debug.Log("Loading Level Additive: " + level);
		isLoadingLevel = true;
		//lastLevelPrefix = levelPrefix;

		// There is no reason to send any more data over the network on the default channel,
		// because we are about to load the level, thus all those objects will get deleted anyway
		Network.SetSendingEnabled(0, false);	

		// We need to stop receiving because first the level must be loaded first.
		// Once the level is loaded, rpc's and other state update attached to objects in the level are allowed to fire
		Network.isMessageQueueRunning = false;

		// All network views loaded from a level will get a prefix into their NetworkViewID.
		// This will prevent old updates from clients leaking into a newly created scene.
		//Network.SetLevelPrefix(levelPrefix);
		Application.LoadLevelAdditive(level);
		//yield return 0;
		//yield return 0;
		yield return StartCoroutine("waitForLoad");

		// Allow receiving data again
		Network.isMessageQueueRunning = true;
		// Now the level has been loaded and we can start sending out data to clients
		Network.SetSendingEnabled(0, true);
		
		addLevelToLoaded(level);
		
		if(player!=null)
		{
			TransportPlayer(player);
			addPlayerToLevel(player.GetComponent<PlayerCharacter>().getName(),level);
		}
		foreach(GameObject go in FindObjectsOfType(typeof(GameObject)) as GameObject[])
		{
			go.SendMessage("OnNetworkLoadedLevel", level, SendMessageOptions.DontRequireReceiver);	
		}
	}
	
	IEnumerator waitForLoad()
	{
		Debug.Log("Waiting for Level to Load");
		while(isLoadingLevel)
		{
			yield return 0;
			yield return 0;
		}
		Debug.Log("Level Loaded");
	}
	
	public void deleteLevelAssets(string level)
	{
		Debug.Log("Deleting level assets: " + level);
		foreach(GameObject go in FindObjectsOfType(typeof(GameObject)) as GameObject[])
		{
			LevelAssetScript assScript = go.GetComponent<LevelAssetScript>();
			if(assScript!=null)
			{
				if(assScript.getLevelName()==level)
				{
					Destroy(go);
				}
			}
		}
		
		removeLevelFromLoaded(level);
	}
	
	//Messages
	void OnServerInitialized()
	{
		Debug.Log("Server Initialised.");
	}
	
	void OnMasterServerEvent(MasterServerEvent mse)
	{
		if(mse==MasterServerEvent.RegistrationSucceeded)
		{
			Debug.Log("Registered Server");
		}
	}
	
	void OnConnectedToServer()
	{
		NextLevel("MasterLevel");
	}
	
	void OnDisconnectedFromServer ()
	{
		//Network.Disconnect();
		Application.LoadLevel(disconnectedLevel);
	}
	
	void OnPlayerDisconnected(NetworkPlayer player) 
	{
		Network.RemoveRPCs(player);
        Network.DestroyPlayerObjects(player);
    }
	
	void OnPlayerConnected(NetworkPlayer player)
	{
		//resetNetScopes();
	}
	
	void OnLevelWasLoaded(int level)
	{
		Debug.Log("OnLevelWasLoaded called.");
		isLoadingLevel = false;
	}
	
	public void levelManagerLoaded()
	{
		Debug.Log("LevelManagerLoaded called");
		isLoadingLevel = false;
	}
	
	void OnNetworkLoadedLevel(string level)
	{
		if(level=="MasterLevel")
		{
			if(Network.isClient)
			{
				playerSpawnedYet=false;
				string destination = SystemScript.getLoggedOutPlaceFromArray();
			
				Debug.Log("Level="+level);
				//Go to level loaded in file or central square
				if(destination=="")
				{
					destination="CentralSquare";	
				}
				
				networkView.RPC("CheckIfLevelLoadedOnServer",RPCMode.Server,destination);
				StartCoroutine(LoadLevelAdd(destination, getLevelPrefix(destination),null));
			}
			else
			{
				SpawnCamera();
			}
		}
		else
		{
			if(Network.isClient)
			{
				if(!playerSpawnedYet)
				{
					Debug.Log("Player being spawned");

					SpawnPlayer();
					playerSpawnedYet=true;
					addPlayerToLevel(SystemScript.getNameFromArray(),level);
				}
			}
		}
	}
	
	void addPlayerToLevel(string character, string level)
	{
		Debug.Log("Adding character: " + character + " to level: " + level + " on server.");
		GameObject[] levelmanagers = GameObject.FindGameObjectsWithTag("LevelManager");
		for(int i=0; i<levelmanagers.Length; i++)
		{
			LevelManager managerScript = levelmanagers[i].GetComponent<LevelManager>();;
			if(managerScript.getLevelName()==level)
			{
				managerScript.addChar(character);
				networkView.RPC("addPlayerToLevelServer",RPCMode.Server,character,level);
			}
		}
	}
	
	public void deletePlayerFromLevel(string character, string level)
	{
		GameObject[] levelmanagers = GameObject.FindGameObjectsWithTag("LevelManager");
		for(int i=0; i<levelmanagers.Length; i++)
		{
			LevelManager managerScript = levelmanagers[i].GetComponent<LevelManager>();
			if(managerScript.getLevelName()==level)
			{
				managerScript.removeChar(character);
				networkView.RPC("deletePlayerFromLevelServer",RPCMode.Server,character,level);
			}
		}
	}
	
	[RPC]
	void addPlayerToLevelServer(string character, string level)
	{
		if(Network.isServer)
		{
			Debug.Log("Adding character: " + character + " to level: " + level + " on server.");
			GameObject[] levelmanagers = GameObject.FindGameObjectsWithTag("LevelManager");
			for(int i=0; i<levelmanagers.Length; i++)
			{
				LevelManager managerScript = levelmanagers[i].GetComponent<LevelManager>();
				if(managerScript.getLevelName()==level)
				{
					managerScript.addChar(character);
				}
			}
		}
	}
	
	[RPC]
	void deletePlayerFromLevelServer(string character, string level)
	{
		if(Network.isServer)
		{
			Debug.Log("Deleting player on server");
			GameObject[] levelmanagers = GameObject.FindGameObjectsWithTag("LevelManager");
			for(int i=0; i<levelmanagers.Length; i++)
			{
				LevelManager managerScript = levelmanagers[i].GetComponent<LevelManager>();
				if(managerScript.getLevelName()==level)
				{
					managerScript.removeChar(character);
				}
			}
		}
	}
	
	void recheckAllLevels()
	{
		GameObject[] levelmanagers = GameObject.FindGameObjectsWithTag("LevelManager");
		for(int i=0; i<levelmanagers.Length; i++)
		{
			LevelManager managerScript = levelmanagers[i].GetComponent<LevelManager>();
			managerScript.checkForEmpty();
		}
	}
	
	[RPC]
	void CloseOffPrevLevelUpdates(string level, NetworkPlayer player)
	{
		changeLevelUpdates(level, player, false);
	}
	
	[RPC]
	void OpenUpPrevLevelUpdates(string level, NetworkPlayer player)
	{
		changeLevelUpdates(level, player,true);
	}
	
	void changeLevelUpdates(string level, NetworkPlayer player, bool changeVal)
	{
		if(Network.isServer)
		{
			foreach(GameObject go in FindObjectsOfType(typeof(GameObject)) as GameObject[])
			{
				if(go.GetComponent<NetworkView>())
				{
					if(go.GetComponent<LevelAssetScript>())
					{
						Debug.Log("Object Level Name= " + go.GetComponent<LevelAssetScript>().getLevelName());
						Debug.Log("Level Passed= " + level);
						if(go.GetComponent<LevelAssetScript>().getLevelName()==level)
						{
							Debug.Log("Closing prev level updates for level: "+level);
							NetworkView netView = go.GetComponent<NetworkView>();
						    netView.SetScope(player, changeVal);
						}
					}
				}
			}
		}
	}
	
	void resetNetScopes()
	{
		if(Network.isServer)
		{
			foreach(GameObject go in FindObjectsOfType(typeof(GameObject)) as GameObject[])
			{
				if(go.GetComponent<NetworkView>())
				{
					NetworkView netView = go.GetComponent<NetworkView>();
					foreach (NetworkPlayer np in Network.connections)
					{
					    netView.SetScope(np, true);
					}
				}
			}
		}
	}
}*/
