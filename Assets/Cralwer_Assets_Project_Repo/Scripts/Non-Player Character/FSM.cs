﻿using UnityEngine;
using System.Collections;

public class FSM
{
	private int m_nCurrentState;
	private int m_nPreviousState;
	
	public FSM()
	{
		m_nCurrentState = 0;
		m_nPreviousState = 0;
	}
	
	public int getCurrentState()
	{
		return m_nCurrentState;
	}
	
	public int getPreviousState()
	{
		return m_nPreviousState;
	}
	
	public void setCurrentState(int nNewState)
	{
		m_nPreviousState = m_nCurrentState;
		m_nCurrentState = nNewState;
	}
}
