﻿using UnityEngine;
using System.Collections;

public class LivingEntity : MonoBehaviour
{
	private int m_nEntityType;
	
	enum tEntityType
	{
		ENTITY_TYPE_UNKNOWN
	};
	
	//////////////////////////////////////////////
	public virtual void Start () 
	{
		setEntityType((int)tEntityType.ENTITY_TYPE_UNKNOWN);
	}
	
	//////////////////////////////////////////////
	public virtual void Update () 
	{
		
	}
	
	//////////////////////////////////////////////
	public void setEntityType(int nType)
	{
		m_nEntityType = nType;
	}
	
	//////////////////////////////////////////////
	public int getEntityType()
	{
		return m_nEntityType;
	}
}
