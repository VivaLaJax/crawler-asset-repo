﻿using UnityEngine;
using System.Collections.Generic;

public class Enemy : GOAPAgent 
{
	private FSM m_fsm;
	public bool m_bDebugLogging = false;
	
	private GameObject m_goTarget;
	
	protected enum tEnemyState
	{
		ENEMY_STATE_NON_COMBAT,
		ENEMY_STATE_COMBAT,
		ENEMY_STATE_ESCAPING
	}
	
	protected int ATTACK_THREAT_LOWER_BOUND = 40;
	protected int ATTACK_THREAT_UPPER_BOUND = 89;
	protected int FLEE_THREAT_THRESHOLD = 90;
	protected int SAFE_THREAT_THRESHOLD = 20;
	
	protected List<int> m_lstThreatExclusion;
	
	private bool m_bHasEscaped;
	
	////////////////////////////////////////////////////////////////////////
	public override void Start () 
	{
		base.Start();
		m_fsm = new FSM();
		m_fsm.setCurrentState((int)tEnemyState.ENEMY_STATE_COMBAT);
		m_bHasEscaped = false;
	}
	
	////////////////////////////////////////////////////////////////////////
	void FixedUpdate()
	{
		
	}
	
	////////////////////////////////////////////////////////////////////////
	public override void Update () 
	{
		if(m_fsm!=null)
		{
			checkFSM();
		}
	}
	
	////////////////////////////////////////////////////////////////////////
	public void checkFSM()
	{
		//base.checkFSM();
		
		int nNewState = assessState(m_fsm.getCurrentState());
		
		if(nNewState!=m_fsm.getCurrentState())
		{
			m_fsm.setCurrentState(nNewState);
		}
		
		switch(m_fsm.getCurrentState())
		{
		case (int)tEnemyState.ENEMY_STATE_NON_COMBAT:
			{
				if(!hasActionPlan())
				{
					HashSet<KeyValuePair<string,object>> hshGoals = decideNonCombatGoals();
					//change this bit to use non-combat goal
					createPlan(getWorldState(), hshGoals);
					performNextAction();
				}
	
				GOAPAction action = m_qActions.Peek();
				if(!action.isDone()) 
				{
					break;
				}
				// the action is done. Remove it so we can perform the next one
				m_qActions.Dequeue();
					
				if(hasActionPlan()) 
				{
					performNextAction();
				} 
				else
				{
					// no actions left, move to Plan state
					characterIdle();
					m_qActions.Clear();
					actionsFinished();
				}
				break;
			}
		case (int)tEnemyState.ENEMY_STATE_COMBAT:
			{
				if(!hasActionPlan())
				{
					HashSet<KeyValuePair<string,object>> hshGoals = decideCombatGoals();
					//change this bit to use combat goal
					createPlan(getWorldState(), hshGoals);
					performNextAction();
				}
	
				GOAPAction action = m_qActions.Peek();
				if(!action.isDone()) 
				{
					break;
				}
				// the action is done. Remove it so we can perform the next one
				m_qActions.Dequeue();
					
				if(hasActionPlan()) 
				{
					performNextAction();
				} 
				else
				{
					// no actions left, move to Plan state
					characterIdle();
					m_qActions.Clear();
					actionsFinished();
				}
				break;	
			}
		case (int)tEnemyState.ENEMY_STATE_ESCAPING:
			{
				/*check current actions*/
				GOAPAction gActCurrAction = m_qActions.Peek();
				if(gActCurrAction!=null)
				{
					if(!gActCurrAction.isDone())
					{
						break;
					}
					else
					{
						m_qActions.Dequeue();
					}
				}
			
				decideMoveAction(m_fsm.getCurrentState(), ref gActCurrAction);
				m_qActions.Enqueue(gActCurrAction);
			
				/*Set off action*/
				m_qActions.Peek().performAction(null);
				break;	
			}
		default:
			{
				Debug.LogError("Enemy FSM in unknown state.");	
				break;
			}
		}
	}
	
	////////////////////////////////////////////////////////////////////////
	public virtual int assessState(int nCurrState)
	{
		List<LivingEntity> lstEntities = checkSenses();
		
		int nHighestThreatLevel = 0;
		int nLowestThreatLevel = 100;
		LivingEntity leHighestThreat = null;
		
		/*for each threat returned*/
		for(int i=0; i<lstEntities.Count; ++i)
		{
			if(!m_lstThreatExclusion.Contains(lstEntities[i].getEntityType()))
			{
				/*assess threat level*/
				//subclass from a threat assessor base used by all NPCs, can then override variables and funcs
				int nThreatLevel = assessThreatLevel(lstEntities[i]);
				
				if(nThreatLevel>nHighestThreatLevel)
				{
					leHighestThreat = lstEntities[i];
					nHighestThreatLevel = nThreatLevel;
				}
				
				if(nThreatLevel<nLowestThreatLevel)
				{
					nLowestThreatLevel = nThreatLevel;
				}
			}
		}
		
		/*is highest threat in attack range?*/
		if(nHighestThreatLevel>=ATTACK_THREAT_LOWER_BOUND && nHighestThreatLevel<=ATTACK_THREAT_UPPER_BOUND)
		{
			m_goTarget = leHighestThreat.gameObject;
			return (int)tEnemyState.ENEMY_STATE_COMBAT;
		}
		/*is highest threat in escape range and we haven't already fled?*/
		if(nHighestThreatLevel>=FLEE_THREAT_THRESHOLD)
		{
			if(m_bHasEscaped)
			{
				return (int)tEnemyState.ENEMY_STATE_COMBAT;
			}
			return (int)tEnemyState.ENEMY_STATE_ESCAPING;
		}
		
		/*if current state was escaping, set escaped if lowest threat is below threshold*/
		if(nCurrState==(int)tEnemyState.ENEMY_STATE_ESCAPING &&
		   					nLowestThreatLevel<=SAFE_THREAT_THRESHOLD ||
		   					!leHighestThreat)
		{
			m_bHasEscaped = true;
			return (int)tEnemyState.ENEMY_STATE_NON_COMBAT;
		}
		
		return nCurrState;
	}
	
	////////////////////////////////////////////////////////////////////////
	public virtual HashSet<KeyValuePair<string,object>> decideNonCombatGoals() 
	{
		Debug.LogError("Base Combat Action Decision called.");
		return null;
	}
	
	////////////////////////////////////////////////////////////////////////
	public virtual HashSet<KeyValuePair<string,object>> decideCombatGoals() 
	{
		Debug.LogError("Base Combat Action Decision called.");
		return null;
	}
	
	////////////////////////////////////////////////////////////////////////
	public virtual void decideMoveAction(int nCurrState, ref GOAPAction moveAction) 
	{
		Debug.LogError("Base Move Action Decision called.");
	}
	
	////////////////////////////////////////////////////////////////////////
	public void setTarget(GameObject goTarget)
	{
		m_goTarget = goTarget;
	}
	
	////////////////////////////////////////////////////////////////////////
	public GameObject getTarget()
	{
		return m_goTarget;
	}
	
	////////////////////////////////////////////////////////////////////////
	public virtual List<LivingEntity> checkSenses()
	{
		/*Check line of site*/
		/*Check circle of hearing*/
		Debug.LogError("Base check senses called.");
		return null;
	}
	
	////////////////////////////////////////////////////////////////////////
	protected void createPlan(HashSet<KeyValuePair<string,object>> worldState,
							  HashSet<KeyValuePair<string,object>> goal)
	{
		// Plan
		Queue<GOAPAction> plan = planner.plan(gameObject, availableActions, worldState, goal);
		if (plan != null) 
		{
			// we have a plan, hooray!
			m_qActions = plan;
			planFound(goal, plan);		
		}
		else 
		{
			// ugh, we couldn't get a plan
			Debug.Log("<color=orange>Failed Plan:</color>"+prettyPrint(goal));
			planFailed(goal);
		}
	}
	
	////////////////////////////////////////////////////////////////////////
	protected void performNextAction()
	{
		// perform the next action
		GOAPAction action = m_qActions.Peek();
		bool bInRange = action.requiresInRange() ? action.isInRange() : true;

		if(bInRange) 
		{
			// we are in range, so perform the action
			bool bSuccess = action.performAction(null);

			if (!bSuccess) 
			{
				// action failed, we need to plan again
				m_qActions.Clear();
				planAborted(action);
			}
		}
		else
		{
			// we need to move there first
			GOAPAction gaMoveAction = null;
			decideMoveAction(m_fsm.getCurrentState(), ref gaMoveAction);
	
			if(gaMoveAction==null)
			{
				// action failed, we need to plan again
				m_qActions.Clear();
				planAborted(action);
			}
			
			Queue<GOAPAction> qNewActions = new Queue<GOAPAction>();
			qNewActions.Enqueue(gaMoveAction);
			
			//transfer the old plan
			while(m_qActions.Count>0)
			{
				qNewActions.Enqueue(m_qActions.Dequeue());
			}
			
			m_qActions = qNewActions;
			
			//set off new first action
			bool bSuccess = m_qActions.Peek().performAction(null);

			if (!bSuccess) 
			{
				// action failed, we need to plan again
				characterIdle();
				m_qActions.Clear();
				planAborted(action);
			}
		}
	}
		
	////////////////////////////////////////////////////////////////////////
	/**
	 * No sequence of actions could be found for the supplied goal.
	 * You will need to try another goal
	 */
	protected override void planFailed (HashSet<KeyValuePair<string,object>> failedGoal)
	{
		Debug.LogError("Base implementation of planFailed called");
	}
	
	////////////////////////////////////////////////////////////////////////
	/**
	 * A plan was found for the supplied goal.
	 * These are the actions the Agent will perform, in order.
	 */
	protected override void planFound (HashSet<KeyValuePair<string,object>> goal, Queue<GOAPAction> actions)
	{
		Debug.Log ("<color=green>Plan found</color> "+prettyPrint(actions));
	}
	
	////////////////////////////////////////////////////////////////////////
	/**
	 * All actions are complete and the goal was reached. Hooray!
	 */
	protected override void actionsFinished ()
	{
		Debug.Log ("<color=blue>Actions completed</color>");
	}
	
	////////////////////////////////////////////////////////////////////////
	/**
	 * One of the actions caused the plan to abort.
	 * That action is returned.
	 */
	protected override void planAborted (GOAPAction aborter)
	{
		Debug.Log ("<color=red>Plan Aborted</color> "+prettyPrint(aborter));
	}
	
	////////////////////////////////////////////////////////////////////////
	protected virtual void characterIdle()
	{
		Debug.LogError("Base implementation of characterIdle called");
	}
}
