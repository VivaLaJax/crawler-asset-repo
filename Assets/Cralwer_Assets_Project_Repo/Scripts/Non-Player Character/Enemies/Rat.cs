﻿using UnityEngine;
using System.Collections.Generic;

public class Rat : Enemy 
{
	private AnimationClip m_AnimIdleClip;
	
	//////////////////////////////////////////////
	public override void Start () 
	{
		base.Start();
		m_AnimIdleClip = gameObject.GetComponent<Animation>().GetClip("Idle");
	}
	
	//////////////////////////////////////////////
	public override List<LivingEntity> checkSenses()
	{
		List<LivingEntity> lstEntities = new List<LivingEntity>();
		
		RaycastHit[] hits;
		
		//hearing circle
		int j =0;
		Vector3 dir = transform.forward;
		dir.y += j;
		hits = Physics.SphereCastAll(transform.position, 10.0f, dir);
	    int i = 0;
	    while (i < hits.Length) 
		{
	    	RaycastHit hit = hits[i];
	        LivingEntity entityScript = hit.collider.GetComponent<LivingEntity>();
	        if (entityScript && !lstEntities.Contains(entityScript)) 
			{
	        	lstEntities.Add(entityScript);
	        }
	        i++;
	    }
				
		return lstEntities;
	}
	
	////////////////////////////////////////////////////////////////////////
	public override HashSet<KeyValuePair<string,object>> decideNonCombatGoals() 
	{
		HashSet<KeyValuePair<string,object>> goal = new HashSet<KeyValuePair<string,object>> ();

		Debug.LogError("Base Combat Action Decision called.");
		return goal;
	}
	
	////////////////////////////////////////////////////////////////////////
	public override HashSet<KeyValuePair<string,object>> decideCombatGoals() 
	{
		HashSet<KeyValuePair<string,object>> goal = new HashSet<KeyValuePair<string,object>> ();

		goal.Add(new KeyValuePair<string, object>(GOAPDefs.GOAL_DEALT_DAMAGE, true ));
		return goal;
	}
	
	////////////////////////////////////////////////////////////////////////
	public override void decideMoveAction(int nCurrState, ref GOAPAction moveAction) 
	{
		switch(nCurrState)
		{
			case (int)tEnemyState.ENEMY_STATE_COMBAT:
			{
				if(getTarget())
				{
					moveAction = gameObject.GetComponent<RunAction>();
					((RunAction)moveAction).setTargetVector(getTarget().transform.position - new Vector3(transform.localScale.z/2,0,transform.localScale.z/2));
				}
				break;
			}
			case (int)tEnemyState.ENEMY_STATE_ESCAPING:
			{
				Debug.Log ("NO FEAR");
				break;
			}
		}
	}
	
	////////////////////////////////////////////////////////////////////////
	public override void loadActions ()
	{
		availableActions.Add(gameObject.AddComponent<RunAction>());
		availableActions.Add(gameObject.AddComponent<ScratchAction>());
		availableActions.Add(gameObject.AddComponent<BiteAction>());
	}
	
	////////////////////////////////////////////////////////////////////////
	/**
	 * The starting state of the Agent and the world.
	 * Supply what states are needed for actions to run.
	 */
	protected override HashSet<KeyValuePair<string,object>> getWorldState ()
	{
		HashSet<KeyValuePair<string,object>> worldState = new HashSet<KeyValuePair<string,object>> ();

		return worldState;
	}
	
	////////////////////////////////////////////////////////////////////////
	public override int assessThreatLevel (LivingEntity lEntity)
	{
		return base.assessThreatLevel(lEntity);
	}
	
	////////////////////////////////////////////////////////////////////////
	protected override void characterIdle()
	{
		AnimationController animController = gameObject.GetComponent<AnimationController>();
	
		if(animController!=null)
		{
			animController.MakeRequest(m_AnimIdleClip.name, 1.0f, 
						(int)AnimationDefs.ControllerRequest.REQ_REPEAT);
		}
	}
}
