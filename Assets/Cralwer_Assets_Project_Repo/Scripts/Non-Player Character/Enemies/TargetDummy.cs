﻿using UnityEngine;
using System.Collections;

public class TargetDummy : MonoBehaviour
{
	public float m_nTotalDamageTaken;
	
	void Start()
	{
		m_nTotalDamageTaken = 0;
	}
	 
	public void OnHit(float nDamage)
	{
		AddDamage(nDamage);
		
		//turn blue
		GetComponent<Renderer>().material.color= new Color(0,0,255);
	}
	
	private void AddDamage(float nDamage)
	{
		m_nTotalDamageTaken += nDamage;
	}
}
