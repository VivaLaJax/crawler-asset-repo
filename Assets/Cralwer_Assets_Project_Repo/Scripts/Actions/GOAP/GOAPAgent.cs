﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GOAPAgent : ThreatAssessor
{
	protected HashSet<GOAPAction> availableActions;
	protected Queue<GOAPAction> m_qActions;

	public GOAPPlanner planner;


	public override void Start () 
	{
		base.Start();
		
		availableActions = new HashSet<GOAPAction> ();
		m_qActions = new Queue<GOAPAction> ();
		planner = new GOAPPlanner ();
		loadActions ();
	}


	public override void Update () 
	{
		
	}


	public void addAction(GOAPAction a) 
	{
		availableActions.Add (a);
	}

	public GOAPAction getAction(Type action) 
	{
		foreach (GOAPAction g in availableActions) 
		{
			if (g.GetType().Equals(action) )
			    return g;
		}
		return null;
	}

	public void removeAction(GOAPAction action) 
	{
		availableActions.Remove (action);
	}

	public bool hasActionPlan() 
	{
		return m_qActions.Count > 0;
	}

	public virtual void loadActions ()
	{
		GOAPAction[] actions = {};//gameObject.GetComponents<GOAPAction>();
		foreach (GOAPAction a in actions) 
		{
			availableActions.Add (a);
		}
		Debug.Log("Found actions: "+prettyPrint(actions));
	}

	public static string prettyPrint(HashSet<KeyValuePair<string,object>> state) 
	{
		String s = "";
		foreach (KeyValuePair<string,object> kvp in state) 
		{
			s += kvp.Key + ":" + kvp.Value.ToString();
			s += ", ";
		}
		return s;
	}

	public static string prettyPrint(Queue<GOAPAction> actions) 
	{
		String s = "";
		foreach (GOAPAction a in actions) 
		{
			s += a.GetType().Name;
			s += "-> ";
		}
		s += "GOAL";
		return s;
	}

	public static string prettyPrint(GOAPAction[] actions) 
	{
		String s = "";
		foreach (GOAPAction a in actions) 
		{
			s += a.GetType().Name;
			s += ", ";
		}
		return s;
	}

	public static string prettyPrint(GOAPAction action) 
	{
		String s = ""+action.GetType().Name;
		return s;
	}
	
	/**
	 * The starting state of the Agent and the world.
	 * Supply what states are needed for actions to run.
	 */
	protected virtual HashSet<KeyValuePair<string,object>> getWorldState ()
	{
		Debug.LogError("Base implementation of getWorldState called");
		return null;
	}

	/**
	 * No sequence of actions could be found for the supplied goal.
	 * You will need to try another goal
	 */
	protected virtual void planFailed (HashSet<KeyValuePair<string,object>> failedGoal)
	{
		Debug.LogError("Base implementation of planFailed called");
	}

	/**
	 * A plan was found for the supplied goal.
	 * These are the actions the Agent will perform, in order.
	 */
	protected virtual void planFound (HashSet<KeyValuePair<string,object>> goal, Queue<GOAPAction> actions)
	{
		Debug.LogError("Base implementation of planFound called");
	}

	/**
	 * All actions are complete and the goal was reached. Hooray!
	 */
	protected virtual void actionsFinished ()
	{
		Debug.LogError("Base implementation of actionsFinished called");
	}

	/**
	 * One of the actions caused the plan to abort.
	 * That action is returned.
	 */
	protected virtual void planAborted (GOAPAction aborter)
	{
		Debug.LogError("Base implementation of planAborted called");
	}
}
