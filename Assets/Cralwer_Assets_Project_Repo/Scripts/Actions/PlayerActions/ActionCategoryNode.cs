using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ActionCategoryNode 
{
	public ActionCategoryNode m_nodeParent;
	public List<ActionCategoryNode> m_lstChildren;
	public string m_strCategoryName;
	public int m_nCategory;
	public string m_strAbilityName;
	
	public ActionCategoryNode() 
	{
		m_strCategoryName = "";
		m_nodeParent = null;
		m_lstChildren = new List<ActionCategoryNode>();
		m_nCategory = 0;
		m_strAbilityName = "";
	}
	
	public List<ActionCategoryNode> GetChildren()
	{
		return m_lstChildren;
	}
	
	public void AddChild(ActionCategoryNode child)
	{
		m_lstChildren.Add(child);
	}
	
	public ActionCategoryNode GetParent()
	{
		return m_nodeParent;
	}
	
	public void SetParent(ActionCategoryNode parent)
	{
		m_nodeParent = parent;
	}
	
	public string GetName()
	{
		return m_strCategoryName;
	}
	
	public void SetName(string name)
	{
		m_strCategoryName = name;
	}
	
	public int GetCategory()
	{
		return m_nCategory;
	}
	
	public void SetCategory(int category)
	{
		m_nCategory = category;
	}
	
	public string GetAbilityName()
	{
		return m_strAbilityName;
	}
	
	public void SetAbilityName(string name)
	{
		m_strAbilityName = name;
	}
}
