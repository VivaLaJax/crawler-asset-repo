using UnityEngine;
using System.Collections;

public class MovementAction : ActionClass
{
	public Transform m_PlayerTransform;
	
	public void OnEnable () 
	{
		m_nActionCategory = (int)ActionCategoryHelper.ActionCategory.ACTION_CATEGORY_MOVEMENT;
	}
	
	public void setTransform(Transform player)
	{
		m_PlayerTransform = player;
	}
}
