using UnityEngine;
using System.Collections;

public class Dash : MovementAction 
{
	bool m_bIsFinished;
	float m_nForwardMoveTime;
	float MAX_FORWARD_MOVE_TIME;
	
	new void OnEnable () 
	{
		base.OnEnable();
		m_bIsFinished = true;
		setActionType((int) ActionType.ACTION_TYPE_BLOCKING);
		setName("Dash");
		MAX_FORWARD_MOVE_TIME = 5.0f;
		m_nForwardMoveTime = MAX_FORWARD_MOVE_TIME;
	}
	
	public override void Update () 
	{
		if(!m_bIsFinished)
		{
			m_PlayerTransform.position = Vector3.Lerp(m_PlayerTransform.position,m_PlayerTransform.forward,0.9f);
			m_nForwardMoveTime-=1.0f;
			
			if(m_nForwardMoveTime<=0)
			{
				m_bIsFinished = true;
			}
		}
	}
	
	public override void executeAction()
	{
		//play animation
		Debug.Log("Dash executed!");
		m_bIsFinished = false;
		m_nForwardMoveTime = MAX_FORWARD_MOVE_TIME;
	}
	
	void OnCollisionEnter(Collision other)
	{
		if(other.gameObject.tag!="Floor")
		{
			m_bIsFinished = true;
			m_nForwardMoveTime = 0;
		}
	}
}
