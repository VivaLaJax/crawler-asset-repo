using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ActionMenuScript : MonoBehaviour 
{
	PlayerCharacterScript m_PlayerScript;
	int m_nMenuIndex;
	List<ActionClass> m_lstPlayerActions;
	
	ActionCategoryNode m_nodeRoot;
	ActionCategoryNode m_nodeCurrent;
	
	void Start () 
	{
		m_lstPlayerActions = new List<ActionClass>();
		m_PlayerScript = GetComponent<PlayerCharacterScript>();
	}
	
	void Update () 
	{
		if(m_PlayerScript.GetInActionMenu())
		{
			if(Input.GetButtonDown("Vertical"))
			{
				m_nMenuIndex = (m_nMenuIndex+1) % m_nodeCurrent.GetChildren().Count;
			}
			else if(Input.GetButtonDown("Vertical"))
			{
				m_nMenuIndex = (m_nMenuIndex-1) % m_nodeCurrent.GetChildren().Count;
			}
			
			//cancel
			if(Input.GetButtonDown("MovementAction"))
			{				
				if(m_nodeCurrent==m_nodeRoot)
				{
					m_PlayerScript.actionMenuToggled();
				}
				else
				{
					m_nodeCurrent = m_nodeCurrent.GetParent();
				}
			}
			
			//approve
			if(Input.GetButtonDown("QueuedAction"))
			{
				ActionCategoryNode action = m_nodeCurrent.GetChildren()[m_nMenuIndex];
				if(action.GetAbilityName()!="")
				{
					if((int) action.GetCategory()==(int)ActionCategoryHelper.ActionCategory.ACTION_CATEGORY_MOVEMENT)
					{
						m_PlayerScript.SetMovementSlot(action.GetAbilityName());
					}
					else if((int)action.GetCategory()==(int)ActionCategoryHelper.ActionCategory.ACTION_CATEGORY_EXECUTABLE)
					{
						m_PlayerScript.SetExecSlot(action.GetAbilityName());
					}
					m_PlayerScript.actionMenuToggled();
				}
				else
				{
					m_nodeCurrent = m_nodeCurrent.GetChildren()[m_nMenuIndex];
					m_nMenuIndex = 0;
				}
			}
		}
	}
	
	void OnGUI()
	{
		if(m_PlayerScript.GetInActionMenu())
		{
			float nBoxWidth = Screen.width * 0.2f;
			float nBoxHeight = Screen.height * 0.3f;
			float nBoxX = Screen.width - nBoxWidth;
			float nBoxY = Screen.height - nBoxHeight;
			
			float nOptionHeight = nBoxHeight * 0.2f;
			float nOptionHeightOffset = nBoxHeight * 0.2f;
			float nOptionWidthOffset = nBoxWidth * 0.1f;
			GUI.Box(new Rect(nBoxX,nBoxY,nBoxWidth,nBoxHeight),"Select An Action");
			
			if(m_nodeCurrent!=null)
			{
				List<ActionCategoryNode> children = m_nodeCurrent.GetChildren();
				for(int i=0; i<children.Count; i++)
				{
					GUI.Box(new Rect(nBoxX+nOptionWidthOffset/2,i*nOptionHeight+nBoxY+nOptionHeightOffset,
						nBoxWidth-nOptionWidthOffset,nOptionHeight), children[i].GetName());
				}
				
				GUI.Box(new Rect(nBoxX+nOptionWidthOffset/2,m_nMenuIndex*nOptionHeight+nBoxY+nOptionHeightOffset,
						nBoxWidth-nOptionWidthOffset,nOptionHeight),"");
			}			
		}
	}
	
	public void buildMenu()
	{
		m_lstPlayerActions = m_PlayerScript.GetAbilityList();
		
		m_nodeRoot = new ActionCategoryNode();
		m_nodeRoot.SetName("Root");
		
		ActionCategoryNode movementNode = new ActionCategoryNode();
		movementNode.SetParent(m_nodeRoot);
		movementNode.SetName("Movement Actions");
		movementNode.SetCategory((int) ActionCategoryHelper.ActionCategory.ACTION_CATEGORY_MOVEMENT);
		ActionCategoryNode execNode = new ActionCategoryNode();
		execNode.SetParent(m_nodeRoot);
		execNode.SetName("Ability Actions");
		execNode.SetCategory((int) ActionCategoryHelper.ActionCategory.ACTION_CATEGORY_EXECUTABLE);
		
		for(int i=0; i<m_lstPlayerActions.Count; i++)
		{
			ActionClass playerAction = m_lstPlayerActions[i];
			
			if(playerAction.GetActionCategory()==movementNode.GetCategory())
			{
				ActionCategoryNode abil = new ActionCategoryNode();
				abil.SetParent(movementNode);
				abil.SetName(playerAction.GetName());
				abil.SetAbilityName(playerAction.GetName());
				abil.SetCategory(playerAction.GetActionCategory());
				movementNode.AddChild(abil);
			}
			else if(playerAction.GetActionCategory()==execNode.GetCategory())
			{
				ActionCategoryNode abil = new ActionCategoryNode();
				abil.SetParent(execNode);
				abil.SetName(playerAction.GetName());
				abil.SetAbilityName(playerAction.GetName());
				abil.SetCategory(playerAction.GetActionCategory());
				execNode.AddChild(abil);
			}
		}
		
		m_nodeRoot.AddChild(movementNode);
		m_nodeRoot.AddChild(execNode);
		
		m_nodeCurrent = m_nodeRoot;
	}
}
