using UnityEngine;
using System.Collections;

public class ExecutableAction : ActionClass
{
	public void OnEnable () 
	{
		m_nActionCategory = (int)ActionCategoryHelper.ActionCategory.ACTION_CATEGORY_EXECUTABLE;
	}
	
	public override void Update () 
	{
	
	}
	
	public override void executeAction()
	{
		
	}
}
