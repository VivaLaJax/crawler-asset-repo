using UnityEngine;
using System.Collections;

public class ActionClass : MonoBehaviour 
{
	public string m_strActionName;
	public int m_nActionType;
	public int m_nActionCategory;
	private bool m_bTimerComplete;
	
	protected AnimationController m_animController;
	
	public virtual void executeAction() {}
	public virtual void Update() {}
	
	public enum ActionType
	{
		ACTION_TYPE_ADDITIVE,
		ACTION_TYPE_BLOCKING
	};
	
	public string GetName()
	{
		return m_strActionName;
	}
	
	public void setName(string strName)
	{
		m_strActionName = strName;
	}
	
	public int GetActionType()
	{
		return m_nActionType;
	}
	
	public void setActionType(int nType)
	{
		m_nActionType = nType;
	}
	
	public int GetActionCategory()
	{
		return m_nActionCategory;
	}
	
	public void setActionCategory(int nCategory)
	{
		m_nActionCategory = nCategory;
	}
	
	public AnimationController getAnimationController()
	{
		return m_animController;
	}
	
	public void setAnimationController(AnimationController animController)
	{
		m_animController = animController;	
	}
	
	public void setTimerComplete(bool bComplete)
	{
		m_bTimerComplete = bComplete;
	}
	
	public bool getTimerComplete()
	{
		return m_bTimerComplete;	
	}
	
	public IEnumerator StartCooldownTimer(float nTime)
	{
		yield return new WaitForSeconds(nTime);
    	m_bTimerComplete = true;
    }
}
