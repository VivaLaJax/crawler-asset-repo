﻿using UnityEngine;
using System.Collections.Generic;

public class BiteAction : GOAPAction
{
	private AnimationClip m_AnimBiteClip;
	private bool m_bComplete;
	
	void Start() 
	{
		setActionType((int) ActionType.ACTION_TYPE_BLOCKING);
		setName("Bite");
		init();
	}
	
	private void init()
	{
		preconditions = new HashSet<KeyValuePair<string, object>> ();
		effects = new HashSet<KeyValuePair<string, object>> ();
		
		addEffect(GOAPDefs.GOAL_DEALT_DAMAGE,true);
		
		setAnimationController(transform.gameObject.GetComponent<AnimationController>());
		m_AnimBiteClip = transform.gameObject.GetComponentInChildren<Animation>().GetClip("Bite");
		reset();
	}
	
	public override void Update () 
	{
		if(getTimerComplete())
		{
			m_bComplete = true;
		}
	}
	
	/**
	 * Reset any variables that need to be reset before planning happens again.
	 */
	public override void reset()
	{
		setTimerComplete(false);
		m_bComplete = false;
	}

	/**
	 * Is the action done?
	 */
	public override bool isDone()
	{
		return m_bComplete;
	}

	/**
	 * Procedurally check if this action can run. Not all actions
	 * will need this, but some might.
	 */
	public override bool checkProceduralPrecondition(GameObject agent)
	{
		target = agent.GetComponent<Enemy>().getTarget();
		return true;
	}

	/**
	 * Run the action.
	 * Returns True if the action performed successfully or false
	 * if something happened and it can no longer perform. In this case
	 * the action queue should clear out and the goal cannot be reached.
	 */
	public override bool performAction(GameObject agent)
	{
		AnimationController animController = getAnimationController();
	
		//EnableCollider((int)tCollider.COLLIDER_LEFT);
		if(animController!=null)
		{
			animController.MakeRequest(m_AnimBiteClip.name, 1.0f, 
						(int)AnimationDefs.ControllerRequest.REQ_PLAY);
			StartCoroutine(StartCooldownTimer(m_AnimBiteClip.length));
		}
		else
		{
			return false;
		}
		
		return true;
	}

	/**
	 * Does this action need to be within range of a target game object?
	 * If not then the moveTo state will not need to run for this action.
	 */
	public override bool requiresInRange ()
	{
		return true;
	}
}
