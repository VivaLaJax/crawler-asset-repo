﻿using UnityEngine;
using System.Collections.Generic;

public class JumpAction : GOAPAction
{
	private AnimationClip m_AnimJumpClip;
	private bool m_bComplete;
	private Vector3 m_vec3Target;
	private bool m_bInProgress;
	
	void Start() 
	{
		setActionType((int) ActionType.ACTION_TYPE_BLOCKING);
		setName("Jump");
		init();
	}
	
	private void init()
	{
		preconditions = new HashSet<KeyValuePair<string, object>> ();
		effects = new HashSet<KeyValuePair<string, object>> ();
		
		addEffect(GOAPDefs.EFFECT_MOVED,true);
		
		setAnimationController(transform.parent.GetComponent<AnimationController>());
		m_AnimJumpClip = transform.parent.GetComponentInChildren<Animation>().GetClip("Jump");
		reset();
	}
	
	public void FixedUpdate()
	{
		if(m_bInProgress && m_vec3Target!=Vector3.zero)
		{
			if(transform.position!=m_vec3Target)
			{
				//lerp towards
				Vector3.Lerp(transform.position, m_vec3Target, 0.2f);
			}
		}
	}
	
	public override void Update () 
	{
		if(m_vec3Target!=Vector3.zero)
		{
			if(transform.position==m_vec3Target)
			{
				m_bComplete = true;
			}
		}
	}
	
	/**
	 * Reset any variables that need to be reset before planning happens again.
	 */
	public override void reset()
	{
		m_bComplete = false;
		setTimerComplete(false);
		m_vec3Target = Vector3.zero;
		m_bInProgress = false;
	}

	/**
	 * Is the action done?
	 */
	public override bool isDone()
	{
		return m_bComplete;
	}

	/**
	 * Procedurally check if this action can run. Not all actions
	 * will need this, but some might.
	 */
	public override bool checkProceduralPrecondition(GameObject agent)
	{
		return true;
	}

	/**
	 * Run the action.
	 * Returns True if the action performed successfully or false
	 * if something happened and it can no longer perform. In this case
	 * the action queue should clear out and the goal cannot be reached.
	 */
	public override bool performAction(GameObject agent)
	{
		AnimationController animController = getAnimationController();
	
		if(animController!=null)
		{
			animController.MakeRequest(m_AnimJumpClip.name, 1.0f, 
						(int)AnimationDefs.ControllerRequest.REQ_PLAY);
		}
		else
		{
			return false;
		}
		
		return true;
	}

	/**
	 * Does this action need to be within range of a target game object?
	 * If not then the moveTo state will not need to run for this action.
	 */
	public override bool requiresInRange ()
	{
		return false;
	}
}
