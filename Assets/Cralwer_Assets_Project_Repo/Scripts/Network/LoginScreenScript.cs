using UnityEngine;
using System.Collections;

public class LoginScreenScript : MonoBehaviour 
{
	string gameName = "VivaLaJax_Crows_Call";
	
	public GameObject netManPrefab;
	GameObject netMan;
	int state; 
	private float btnX;
	private float btnY;
	private float btnW;
	private float btnH;
	string toSend;
	
	private HostData[] hostInfo;
	private bool refreshing = false;
	
	private int partySize;
	
	public enum LoginState
	{
		STATE_CHECK_NAME,
		STATE_NAME_INPUT,
		STATE_NAME_RECEIVED,
		STATE_GET_PARTY_SIZE,
		STATE_SEND_PARTY_SIZE
	}
	
	void Start () 
	{
		state = (int) LoginState.STATE_NAME_RECEIVED;
		
		netMan = GameObject.FindGameObjectWithTag("NetworkManager");
		
		if(netMan!=null)
		{
			Destroy(netMan);
		}
		
		Instantiate(netManPrefab);
		
		btnX = (float)(Screen.width * 0.05);
		btnY = (float)(Screen.width * 0.05);
		btnW = (float)(Screen.width * 0.1);
		btnH = (float)(Screen.width * 0.1);
		toSend = "";
		hostInfo = null;
		partySize = 0;
	}
	
	void Update()
	{
		switch(state)
		{
			//check if you can get a name from file
			//if so move into netmanager
			case (int) LoginState.STATE_CHECK_NAME:
			{	
				//if(SystemScript.getNameFromArray()!="")
				//{
					state = (int) LoginState.STATE_NAME_RECEIVED;
				//}
				//else
				//{
				//	state = (int) LoginState.STATE_NAME_INPUT;
				//}
			
				break;
			}
			//get the name from input and create a file
			case (int) LoginState.STATE_NAME_INPUT:
			case (int) LoginState.STATE_GET_PARTY_SIZE:
			{
				//show a text box for inputting text, with string in it
				foreach (char c in Input.inputString) 
				{
           			if (c == "\b"[0])
					{
               			if (toSend.Length != 0)
						{
                   			toSend = toSend.Substring(0, toSend.Length - 1);
						}
					}
					else
					{
						toSend+=c;
					}
				}
			
				break;
			}
			//nothing let netman take over
			case (int) LoginState.STATE_NAME_RECEIVED:
			{
				//nada right now
				break;
			}
		}
		
		if(refreshing)
		{
			if(MasterServer.PollHostList().Length>0)
			{
				refreshing = false;
				hostInfo = MasterServer.PollHostList();
			}
		}
	}
	
	public int getCurrentState()
	{
		return state;
	}
	
		
	void RefreshHostList()
	{
		MasterServer.RequestHostList(gameName);
		refreshing = true;
	}
	
	//GUI
	void OnGUI () 
	{
		if (!Network.isClient && !Network.isServer && Application.loadedLevelName=="LoginScreen")
		{			
			if(state==(int) LoginState.STATE_NAME_INPUT)
			{
				//show box with text being typed here
				GUI.Box(new Rect(btnX,btnY,btnW,btnH), toSend);
				
				if(GUI.Button(new Rect(btnX,(float)(btnY*1.2+btnH),btnW,btnH), "Enter Name"))
				{
					//create file and save name to it
					//SystemScript.saveVarToArray(toSend, (int) SystemScript.StoredVariables.VAR_NAME);
					//change to state received
					state = (int) LoginState.STATE_NAME_RECEIVED;
				}
			}
			else if(getCurrentState()==(int)LoginState.STATE_NAME_RECEIVED)
			{
				if(GUI.Button(new Rect(btnX,btnY,btnW,btnH), "Start Server"))
				{
					Debug.Log("Starting Server");
					state = (int) LoginState.STATE_GET_PARTY_SIZE;
					toSend = "";
				}
		
				if(GUI.Button(new Rect(btnX,(float)(btnY*1.2+btnH),btnW,btnH), "Refresh Hosts"))
				{
					Debug.Log("Refreshing");
					RefreshHostList();
				}
			
				if(hostInfo!=null)
				{
					if(hostInfo.Length>0)
					{
						for(int i=0; i<hostInfo.Length; i++)
						{
							if(GUI.Button(new Rect(btnX*1.5f + btnW,btnY*1.2f+(btnH*i),btnW*3,btnH*0.5f), hostInfo[i].gameName))
							{
								Network.Connect(hostInfo[i]);
								Debug.Log("Joining Server.");
							}
						}
					}
				}
			}
			if(state==(int) LoginState.STATE_GET_PARTY_SIZE)
			{
				//show box with text being typed here
				GUI.Box(new Rect(btnX,btnY,btnW,btnH), toSend);
				
				if(GUI.Button(new Rect(btnX,(float)(btnY*1.2+btnH),btnW,btnH), "Enter Party Size"))
				{
					//create file and save name to it
					//SystemScript.saveVarToArray(toSend, (int) SystemScript.StoredVariables.VAR_NAME);
					//change to state received
					partySize = int.Parse(toSend);
					if(partySize<1)
					{
						partySize = 1;
					}
					
					if(partySize>4)
					{
						partySize = 4;
					}
					
					state = (int) LoginState.STATE_SEND_PARTY_SIZE;
				}
			}
			if(state==(int) LoginState.STATE_SEND_PARTY_SIZE)
			{					
				NetworkManagerScript netManScript = 
					GameObject.FindGameObjectWithTag("NetworkManager").GetComponent<NetworkManagerScript>();
				netManScript.StartServer(gameName, partySize);
			}
		}
	}
}
