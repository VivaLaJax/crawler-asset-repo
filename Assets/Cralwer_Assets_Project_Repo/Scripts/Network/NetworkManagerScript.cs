using UnityEngine;
using System.Collections;

public class NetworkManagerScript : MonoBehaviour 
{
//	private float btnX;
//	private float btnY;
//	private float btnW;
//	private float btnH;
	
	public Transform spawnObject;
	public GameObject camPrefab;
	public GameObject playerPrefab;
	
	public string[] supportedNetworkLevels = { "LoginScreen","MasterLevel","DungeonLevel0","DungeonLevel1"};
	public ArrayList strArrLloadedLevels = new ArrayList();
	
	string  disconnectedLevel = "LoginScreen";
//	private int lastLevelPrefix = 0;
	bool isLoadingLevel;
	
	string destination;
	bool playerSpawnedYet;
	
	NetworkPlayer serverInfo;
	
	//Unity standard procedures
	void Awake()
	{
		DontDestroyOnLoad(this);
	}
	
	void Start()
	{
//		btnX = (float)(Screen.width * 0.05);
//		btnY = (float)(Screen.width * 0.05);
//		btnW = (float)(Screen.width * 0.1);
//		btnH = (float)(Screen.width * 0.1);
		isLoadingLevel = false;
		
		destination="";
	}	
	
	////////////////////////////////////SERVER&CLIENT CODE////////////////////////////////////////////////	
	public NetworkPlayer getServerInfo()
	{
		return serverInfo;
	}
	
	public LevelManager getLevelManager(string levelName)
	{
		GameObject[] managers = GameObject.FindGameObjectsWithTag("LevelManager");
		for(int i=0; i<managers.Length; i++)
		{
			if(managers[i].GetComponent<LevelManager>().getLevelName()==levelName)
			{
				return managers[i].GetComponent<LevelManager>();
			}
		}
		
		return null;
	}
	
	[RPC]
	public void RequestChangeLevelIndex(int prevIndex, int nextIndex, NetworkPlayer player)
	{
		Debug.Log ("Requested change of Level index: " + nextIndex);
		if(nextIndex+2<supportedNetworkLevels.Length && nextIndex>=0)
		{
			string level = supportedNetworkLevels[nextIndex+2];

			if(!Network.isServer)
			{
				Debug.Log("Requesting level creation on Server for level: " + level);
				GetComponent<NetworkView>().RPC("RequestChangeLevelIndex",getServerInfo(),prevIndex, nextIndex,Network.player);
			}
			StartCoroutine(ChangeLevel(prevIndex+2,nextIndex+2,player));
		}
	}
	
	[RPC]
	public void RequestChangeLevelString(string destination, NetworkPlayer player)
	{
		Debug.Log ("Requested change of Level name: " + destination);
		
		int prevIndex = -99;
		int nextIndex = -99;
		for(int i=0; i<supportedNetworkLevels.Length; i++)
		{
			if(supportedNetworkLevels[i]==destination)
			{
				nextIndex = i;
				break;
			}
		}
		
		if(nextIndex==-99)
		{
			Debug.Log("Destination not found");
			return;
		}
		
		if(nextIndex<supportedNetworkLevels.Length && nextIndex>=0)
		{
			if(!Network.isServer)
			{
				GetComponent<NetworkView>().RPC("RequestChangeLevelIndex",getServerInfo(),prevIndex-2, nextIndex-2,Network.player);
			}
			StartCoroutine(ChangeLevel(prevIndex,nextIndex,player));
		}
	}

	public IEnumerator ChangeLevel(int prevIndex, int nextIndex, NetworkPlayer player)
	{
		string prevLevel = "";
		if(prevIndex!=-99)
		{
			prevLevel = supportedNetworkLevels[prevIndex];
		}
		string level = supportedNetworkLevels[nextIndex];
		LevelManager manager;
		GameObject playerObj;
		
		if(!Network.isServer)
		{
			if(!isLevelLoaded(level))
			{
				Debug.Log("Client: About to load level: " + level + " (" + nextIndex + ")");
				StartCoroutine(LoadLevelAdd(level, getLevelPrefix(level)));
				yield return StartCoroutine("waitForLoad");
				
				manager = getLevelManager(level);
				Debug.Log("Level Manager found");
				//add player to level
				manager.addChar(player);
				//remove player from prev
				if(prevLevel!="")
				{
					getLevelManager(prevLevel).removeChar(player);
				}
				//transport player!!!!!!!!!!!!!!!!!
				playerObj = GetPlayerObjectFromNetworkPlayer(player);
				if(playerObj!=null)
				{
					playerObj.transform.position = manager.transform.position;
				}
			}
		}
		else
		{
			Debug.Log("Server ChangeLevel called");
			if(isLevelLoaded(level))
			{
				Debug.Log("ChangeLevel is already loaded");
				manager = getLevelManager(level);
				GetComponent<NetworkView>().RPC("PropagateID",player,manager.getViewId(),level);
				manager.createNetObjs(player);
				//add player to level
				manager.addChar(player);
				//remove player from prev
				if(prevLevel!="")
				{
					Debug.Log("PrevLevel: " + prevLevel);
					getLevelManager(prevLevel).removeChar(player);
					changeNetScopesByLevel(prevLevel,player,false);
				}
				changeNetScopesByLevel(level,player,true);
			}
			else
			{
				Debug.Log("ChangeLevel is being loaded");
				ServerLoadLevelAdd(level);
				yield return StartCoroutine("waitForLoad");
				
				addLevelToLoaded(level);
				manager = getLevelManager(level);
				GetComponent<NetworkView>().RPC("PropagateID",player,manager.getViewId(),level);
				//do level man setup
				manager.Setup();
				manager.createNetObjs(player);
				//add player to level
				manager.addChar(player);
				//remove player from prev
				if(prevLevel!="")
				{
					Debug.Log("PrevLevel: " + prevLevel);
					getLevelManager(prevLevel).removeChar(player);
				}
				//Disable new ids!!!!!!!!!!!!!!!
				disableNewIdsForLevel(level,player);
			}
		}
		
		playerObj = GetPlayerObjectFromNetworkPlayer(player);
		if(playerObj!=null)
		{
			playerObj.GetComponent<PlayerCharacterScript>().SetCurrentLevel(nextIndex-2);
		}
	}

	void NextLevel(string level)
	{
		Network.RemoveRPCsInGroup(0);
		Network.RemoveRPCsInGroup(1);
		
		StartCoroutine(LoadLevel(level, getLevelPrefix(level)));
		addLevelToLoaded(level);
	}
	
	public IEnumerator LoadLevel(string level, int levelPrefix)
	{
		isLoadingLevel = true;
//		lastLevelPrefix = levelPrefix;

		// There is no reason to send any more data over the network on the default channel,
		// because we are about to load the level, thus all those objects will get deleted anyway
		Network.SetSendingEnabled(0, false);	

		// We need to stop receiving because first the level must be loaded first.
		// Once the level is loaded, rpc's and other state update attached to objects in the level are allowed to fire
		Network.isMessageQueueRunning = false;

		// All network views loaded from a level will get a prefix into their NetworkViewID.
		// This will prevent old updates from clients leaking into a newly created scene.
		Network.SetLevelPrefix(levelPrefix);
		Application.LoadLevel(level);
		Debug.Log ("Level loaded.");
		yield return StartCoroutine("waitForLoad");

		// Allow receiving data again
		Network.isMessageQueueRunning = true;
		// Now the level has been loaded and we can start sending out data to clients
		Network.SetSendingEnabled(0, true);
		
		foreach(GameObject go in FindObjectsOfType(typeof(GameObject)) as GameObject[])
		{
			go.SendMessage("OnNetworkLoadedLevel", level, SendMessageOptions.DontRequireReceiver);	
		}
	}
		
	public IEnumerator LoadLevelAdd(string level, int levelPrefix)
	{
		Debug.Log("Loading Level Additive: " + level);
		isLoadingLevel = true;
//		lastLevelPrefix = levelPrefix;

		// There is no reason to send any more data over the network on the default channel,
		// because we are about to load the level, thus all those objects will get deleted anyway
		Network.SetSendingEnabled(0, false);	

		// We need to stop receiving because first the level must be loaded first.
		// Once the level is loaded, rpc's and other state update attached to objects in the level are allowed to fire
		Network.isMessageQueueRunning = false;

		// All network views loaded from a level will get a prefix into their NetworkViewID.
		// This will prevent old updates from clients leaking into a newly created scene.
		Network.SetLevelPrefix(levelPrefix);
		Application.LoadLevelAdditive(level);

		
		yield return StartCoroutine("waitForLoad");

		// Allow receiving data again
		Network.isMessageQueueRunning = true;
		// Now the level has been loaded and we can start sending out data to clients
		Network.SetSendingEnabled(0, true);
		
		
		foreach(GameObject go in FindObjectsOfType(typeof(GameObject)) as GameObject[])
		{
			go.SendMessage("OnNetworkLoadedLevel", level, SendMessageOptions.DontRequireReceiver);	
		}
	}
	
	int getLevelPrefix(string level)
	{
		for(int i=0; i<supportedNetworkLevels.Length; i++)
		{
			if(supportedNetworkLevels[i]==level)
			{
				return i;
			}
		}
		
		return -99;
	}

	IEnumerator waitForLoad()
	{
		Debug.Log("Waiting for Level to Load");
		while(isLoadingLevel)
		{
			yield return 0;
			yield return 0;
		}
		Debug.Log("Level Loaded");
	}
	
	public void levelManagerLoaded()
	{
		Debug.Log("LevelManagerLoaded called.");
		isLoadingLevel = false;
	}
	
	void SpawnCamera()
	{
		spawnObject = GameObject.FindGameObjectWithTag("CameraSpawn").transform;
		Instantiate(camPrefab, spawnObject.position, spawnObject.rotation);
	}
	
	public bool isLevelLoaded(string level)
	{
		return strArrLloadedLevels.Contains(level);
	}
	
	public void addLevelToLoaded(string level)
	{
		Debug.Log("Level added: " + level);
		strArrLloadedLevels.Add(level);
	}
	
	public void removeLevelFromLoaded(string level)
	{
		Debug.Log("Level removed: " + level);
		strArrLloadedLevels.Remove(level);
		for(int i=0; i<strArrLloadedLevels.Count; i++)
		{
			Debug.Log("Loaded Level: " + strArrLloadedLevels[i]);
		}
	}
	
	public string GetLevelNameFromIndex(int index)
	{
		return supportedNetworkLevels[index];
	}
	
	GameObject GetPlayerObjectFromNetworkPlayer(NetworkPlayer player)
	{
		GameObject[] playerObjs = GameObject.FindGameObjectsWithTag("Player");
		for(int i=0; i<playerObjs.Length; i++)
		{
			if(playerObjs[i].GetComponent<PlayerCharacterScript>().GetOwner()==player)
			{
				return playerObjs[i];
			}
		}
		
		return null;
	}
	////////////////////////////////////SERVER CODE////////////////////////////////////////////////	
	public void StartServer(string gameName, int partySize)
	{
		NetworkConnectionError error = Network.InitializeServer(partySize,251,!Network.HavePublicAddress());
		MasterServer.RegisterHost(gameName,"Learning Game: Jax","Tutorial Game");
		
		serverInfo = Network.player;
		
		if(error.ToString()=="NoError")
		{
			NextLevel("MasterLevel");
		}	
	}
	
	[RPC]
	void ServerLoadLevelAdd(string destination)
	{
		if(!isLevelLoaded(destination))
		{
			StartCoroutine(LoadLevelAdd(destination, getLevelPrefix(destination)));
		}
	}
	
	[RPC]
	void RequestPlayerSpawn(NetworkPlayer player, string level)
	{
		NetworkViewID playId = Network.AllocateViewID();
		SpawnPlayer(player,playId,level);
		for(int i=0; i<Network.connections.Length; i++)
		{
			GetComponent<NetworkView>().RPC("SpawnPlayer",Network.connections[i],player,playId,level);
		}
	}
	
	[RPC]
	public void PropagateID(NetworkViewID viewId, string level)
	{
		Debug.Log("Propagating ID to Level: " + level);
		LevelManager manager = getLevelManager(level);
		manager.recieveId(viewId);
	}
	
	public NetworkViewID GetNextId()
	{
		return Network.AllocateViewID();
	}
	
	void changeNetScopesByLevel(string level, NetworkPlayer player, bool changeTo)
	{
		foreach(GameObject go in FindObjectsOfType(typeof(GameObject)) as GameObject[])
		{
			if(go.GetComponent<NetworkView>()!=null)
			{
				LevelAssetScript levelScript = go.GetComponent<LevelAssetScript>();
				if(levelScript!=null)
				{
					if(levelScript.getLevelName()==level)
					{
						go.GetComponent<NetworkView>().SetScope(player, changeTo);
					}
				}
				else
				{
					if(go.gameObject.tag=="Player")
					{
						PlayerCharacterScript charScript = go.GetComponent<PlayerCharacterScript>();
						Debug.Log("PLayer Level: " + supportedNetworkLevels[charScript.GetCurrentLevel()]+2);
						Debug.Log("Level check: " + level);
						if(supportedNetworkLevels[charScript.GetCurrentLevel()+2]==level)
						{
							go.GetComponent<NetworkView>().SetScope(player, changeTo);
						}
					}
				}
			}
		}
	}
	
	void turnOffAllScopes(NetworkPlayer player)
	{
		foreach(GameObject go in FindObjectsOfType(typeof(GameObject)) as GameObject[])
		{
			if(go.GetComponent<NetworkView>()!=null)
			{
				go.GetComponent<NetworkView>().SetScope(player, false);
			}
		}
	}

	void disableNewIdsForLevel(string level, NetworkPlayer player)
	{
		for(int i=0; i<Network.connections.Length; i++)
		{
			if(Network.connections[i]!=player)
			{
				changeNetScopesByLevel(level,Network.connections[i],false);
			}
		}
	}
	
	public void deleteLevelAssets(string level)
	{
		Debug.Log("Deleting level assets: " + level);
		foreach(GameObject go in FindObjectsOfType(typeof(GameObject)) as GameObject[])
		{
			LevelAssetScript assScript = go.GetComponent<LevelAssetScript>();
			if(assScript!=null)
			{
				if(assScript.getLevelName()==level)
				{
					Destroy(go);
				}
			}
			else
			{
				if(!Network.isServer)
				{
					PlayerCharacterScript playerScript = go.GetComponent<PlayerCharacterScript>();
					if(playerScript!=null)
					{
						Debug.Log("Deleting Assets: Player Level: " + supportedNetworkLevels[playerScript.GetCurrentLevel()+2]);
						Debug.Log("Deleting Assets: Level : " + level);
						Debug.Log("Deleting Assets: Owner: " + playerScript.GetOwner());
						if(supportedNetworkLevels[playerScript.GetCurrentLevel()+2]==level && playerScript.GetOwner()!=Network.player)
						{
							Destroy(go);
						}
					}
				}
			}
		}
		
		removeLevelFromLoaded(level);
	}
	
	public bool ArePlayersInSameLevel(NetworkPlayer otherPlayer, string currLevel)
	{
		LevelManager levelMan = getLevelManager(currLevel);
		
		if(levelMan.isCharacterInLevel(otherPlayer))
		{
			return true;
		}
		
		return false;
	}
	
	////////////////////////////////////CLIENT CODE////////////////////////////////////////////////	
	[RPC]
	void ProvideServerInfo(NetworkPlayer player)
	{
		Debug.Log("ProvideServerInfo has been called");
		if(Network.isClient)
		{
			Debug.Log("Client: Got Server Info");
			serverInfo = player;
		}
	}
	
	[RPC]
	public void SpawnPlayer(NetworkPlayer player, NetworkViewID playId, string level)
	{
		Vector3 loadedPos = new Vector3(0,5,0);

		GameObject playerObject = (GameObject)Instantiate(playerPrefab, loadedPos, Quaternion.identity);
		NetworkView view = playerObject.GetComponent<NetworkView>();
		view.viewID = playId;
		playerObject.GetComponent<PlayerCharacterScript>().SetOwner(player);
		playerObject.GetComponent<PlayerCharacterScript>().SetCurrentLevel(getLevelPrefix(level)-2);
		
	}
	
	public void DestroyPlayer(NetworkPlayer player)
	{
		GameObject[] playerObjs = GameObject.FindGameObjectsWithTag("Player");
		for(int i=0; i<playerObjs.Length; i++)
		{
			if(playerObjs[i].GetComponent<PlayerCharacterScript>().GetOwner()==player)
			{
				Destroy(playerObjs[i]);
				return;
			}
		}
	}
	
	[RPC]
	void deleteDiscPlayerObjects(int index)
	{
		GameObject[] playerObjs = GameObject.FindGameObjectsWithTag("Player");
		Destroy(playerObjs[index]);
	}
	
	////////////////////////////////////////////Messages/////////////////////////////////////////////
	void OnServerInitialized()
	{
		Debug.Log("Server Initialised.");
	}
	
	void OnMasterServerEvent(MasterServerEvent mse)
	{
		if(mse==MasterServerEvent.RegistrationSucceeded)
		{
			Debug.Log("Registered Server");
		}
	}
	
	void OnConnectedToServer()
	{
		Debug.Log("Client: Connected to Server");
		NextLevel("MasterLevel");
	}
	
	void OnDisconnectedFromServer ()
	{
		Application.LoadLevel(disconnectedLevel);
	}
	
	void OnPlayerDisconnected(NetworkPlayer player) 
	{
		Network.RemoveRPCs(player);
		int playerIndex = -99;
		
        GameObject[] playerObjs = GameObject.FindGameObjectsWithTag("Player");
		for(int i=0; i<playerObjs.Length; i++)
		{
			if(playerObjs[i].GetComponent<PlayerCharacterScript>().GetOwner()==player)
			{
				playerIndex = i;
			}
		}
		
		int level = playerObjs[playerIndex].GetComponent<PlayerCharacterScript>().GetCurrentLevel();
		getLevelManager(GetLevelNameFromIndex(level+2)).removeChar(player);
			
		if(playerIndex!=-99)
		{
			Destroy(playerObjs[playerIndex]);
			for(int i=0; i<Network.connections.Length; i++)
			{
				GetComponent<NetworkView>().RPC("deleteDiscPlayerObjects",Network.connections[i],playerIndex);
			}
		}
    }
	
	void OnPlayerConnected(NetworkPlayer player)
	{
		if(Network.isServer)
		{
			turnOffAllScopes(player);
			Debug.Log("Server: Player has connected.");
			GetComponent<NetworkView>().RPC("ProvideServerInfo",player,serverInfo);
			Debug.Log("Server: Sent Server Info");
		}
	}
	
	void OnLevelWasLoaded(int level)
	{
		Debug.Log("OnLevelWasLoaded called.");
		isLoadingLevel = false;
	}
	
	void OnNetworkLoadedLevel(string level)
	{
		if(level=="MasterLevel")
		{
			if(Network.isServer)
			{
				//spawn camera to watch the world
				SpawnCamera();
			}
			else
			{
				//if you're the client, load up the level to play
				playerSpawnedYet=false;
			
				Debug.Log("Level="+level);
				//Go to level loaded in file or central square
				if(destination=="")
				{
					destination="DungeonLevel0";
				}
				
				if(!isLevelLoaded(destination))
				{
					RequestChangeLevelString(destination, Network.player);
				}
			}
		}
		else
		{
			Debug.Log("NetLoadLevel: Level="+level);
			Debug.Log("NetLoadLevel: Network Player="+Network.player.ToString());
			
			if(Network.isClient)
			{
				if(!playerSpawnedYet)
				{
					Debug.Log("Player being spawned");

					GetComponent<NetworkView>().RPC("RequestPlayerSpawn",serverInfo,Network.player,level);
					playerSpawnedYet=true;
				}
			}
		}
	}
}
